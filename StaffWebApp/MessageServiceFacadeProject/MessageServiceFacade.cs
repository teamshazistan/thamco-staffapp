﻿using MessageServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MessageServiceFacadeProject
{
    #region Interface
    public interface IMessageServiceFacade
    {
        IEnumerable<MS_Messages> getMessagesFromRecipientID(string recipientID);
        IEnumerable<MS_Messages> getMessagesFromSenderID(string senderID);
        MS_Messages sendMessageToRecipient(MS_SendMessageDTO sentMessage);
    }
    #endregion

    #region Message Facade
    public class MessageServiceFacade : IMessageServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        #region Constructors
        public MessageServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MessageServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public MessageServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }
        #endregion

        public IEnumerable<MS_Messages> getMessagesFromRecipientID(string recipientID)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/Messages?recipientID={0}", recipientID);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<MS_Messages>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public IEnumerable<MS_Messages> getMessagesFromSenderID(string senderID)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/MessagesBySender?senderID={0}", senderID);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<MS_Messages>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public MS_Messages sendMessageToRecipient(MS_SendMessageDTO sentMessage)
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync("api/SendMessage", sentMessage).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<MS_Messages>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
    }
    #endregion

    #region Message Test Facade
    public class MessageServiceFacade_test : IMessageServiceFacade
    {
        private List<MS_Messages> testMessages;
        private List<MS_Recipients> testRecipients;

        public MessageServiceFacade_test()
        {
            testMessages = new List<MS_Messages>()
            {
                new MS_Messages { Id = 0, SenderNo = "P44_54", Contents = "Message 1" },
                new MS_Messages { Id = 1, SenderNo = "P44_54", Contents = "Message 2" },
                new MS_Messages { Id = 2, SenderNo = "P86_44", Contents = "Message 3" }
            };

            testRecipients = new List<MS_Recipients>()
            {
                new MS_Recipients {Id = 0, Message = testMessages[0], RecipientNo = "P44_54" },
                new MS_Recipients {Id = 1, Message = testMessages[1], RecipientNo = "P44_54" },
                new MS_Recipients {Id = 2, Message = testMessages[2], RecipientNo = "P86_44" }
            };
        }

        public IEnumerable<MS_Messages> getMessagesFromRecipientID(string recipientID)
        {
            return testRecipients.Where(x => x.RecipientNo == recipientID).Select(x => x.Message);
        }

        public IEnumerable<MS_Messages> getMessagesFromSenderID(string senderID)
        {
            return testMessages.Where(x => x.SenderNo == senderID);
        }

        public MS_Messages sendMessageToRecipient(MS_SendMessageDTO sentMessage)
        {
            var newMessage = new MS_Messages()
            {
                Contents = sentMessage.Contents,
                SenderNo = sentMessage.SenderNo
            };
            testMessages.Add(newMessage);

            foreach (string recipient in sentMessage.Recipients)
            {
                testRecipients.Add(new MS_Recipients()
                {
                    Message = newMessage,
                    RecipientNo = recipient
                });
            }
            return newMessage;
        }
    }
    #endregion

}
