﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageServiceFacadeProject.DTOs
{
    public class MS_Messages
    {
        public int Id { get; set; }
        public string SenderNo { get; set; }
        public string Contents { get; set; }
    }
}
