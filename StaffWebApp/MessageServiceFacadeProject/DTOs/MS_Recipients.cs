﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageServiceFacadeProject.DTOs
{
    public class MS_Recipients
    {
        public int Id { get; set; }
        public MS_Messages Message { get; set; }
        public string RecipientNo { get; set; }
    }
}
