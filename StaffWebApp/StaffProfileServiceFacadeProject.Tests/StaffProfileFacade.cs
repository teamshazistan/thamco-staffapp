﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace StaffProfileServiceFacadeProject.Tests
{
    [TestClass]
    public class StaffProfileFacade
    {
        [TestMethod]
        public void TestStaffProfileConnection()
        {
            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["StaffProfileServiceURL"];

            //Act
            StaffProfileServiceFacade facade1 = new StaffProfileServiceFacade(badURL1);
            StaffProfileServiceFacade facade2 = new StaffProfileServiceFacade(badURL2);
            StaffProfileServiceFacade facade3 = new StaffProfileServiceFacade(badURL3);
            StaffProfileServiceFacade facade4 = new StaffProfileServiceFacade(badURL4);
            StaffProfileServiceFacade facade5 = new StaffProfileServiceFacade(goodURL1);

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);
        }
    }
}
