﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace PurchasingServiceFacadeProject.Tests
{
    [TestClass]
    public class PurchasingFacade
    {
        [TestMethod]
        public void TestPurchasingConnection()
        {
            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["PurchasingServiceURL"];

            //Act
            PurchasingServiceFacade facade1 = new PurchasingServiceFacade(badURL1);
            PurchasingServiceFacade facade2 = new PurchasingServiceFacade(badURL2);
            PurchasingServiceFacade facade3 = new PurchasingServiceFacade(badURL3);
            PurchasingServiceFacade facade4 = new PurchasingServiceFacade(badURL4);
            PurchasingServiceFacade facade5 = new PurchasingServiceFacade(goodURL1);

            var invoices1 = facade1.getPendingInvoices();
            var invoices2 = facade2.getPendingInvoices();
            var invoices3 = facade3.getPendingInvoices();
            var invoices4 = facade4.getPendingInvoices();
            var invoices5 = facade5.getPendingInvoices();

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);

            Assert.IsNull(invoices1);
            Assert.IsNull(invoices2);
            Assert.IsNull(invoices3);
            Assert.IsNull(invoices4);
            //Assert.IsNotNull(invoices5); //Requires service to be up and running. Should it be removed due to dependancy.
        }
    }
}
