﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCardServiceFacadeProject.DTOs
{
    public class CC_CreditCard
    {
        public string cardNumber { get; set; }
        public string rawCardNumber { get; set; }
        public string hashCard { get; set; }
        public string nameOnCard { get; set; }
        public bool active { get; set; }
    }
}
