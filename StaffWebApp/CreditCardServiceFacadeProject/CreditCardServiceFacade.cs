﻿using CreditCardServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CreditCardServiceFacadeProject
{
    public interface ICreditCardServiceFacade
    {
        IEnumerable<CC_CreditCard> getCreditCards();
        IEnumerable<string> getCreditCardString();
    }

    public class CreditCardServiceFacade : ICreditCardServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        public CreditCardServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["CreditCardServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public CreditCardServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public IEnumerable<CC_CreditCard> getCreditCards()
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync("CC/GetStaffCards").Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<CC_CreditCard>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public IEnumerable<string> getCreditCardString()
        {
            var creditCardObj = getCreditCards();
            if (creditCardObj != null)
                return getCreditCards().Select(x => String.Format("{0} - {1}", x.cardNumber, x.nameOnCard));
            return null;
        }
    }

    public class CreditCardServiceFacade_test : ICreditCardServiceFacade
    {
        private IEnumerable<CC_CreditCard> testCreditCards;

        public CreditCardServiceFacade_test()
        {
            testCreditCards = new List<CC_CreditCard>()
            {
                new CC_CreditCard() { active = true, cardNumber = "************4567", hashCard = "-802938376", nameOnCard = "Mr T hAmCo                    ",},
                new CC_CreditCard() { active = true, cardNumber = "************7654", hashCard = "515282330", nameOnCard = "M J Leon                      ",}
            };

        }

        public IEnumerable<CC_CreditCard> getCreditCards()
        {
            return testCreditCards;
        }

        public IEnumerable<string> getCreditCardString()
        {
            var creditCardObj = getCreditCards();
            if (creditCardObj != null)
                return getCreditCards().Select(x => String.Format("{0} - {1}", x.cardNumber, x.nameOnCard));
            return null;
        }
    }
}
