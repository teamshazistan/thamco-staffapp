﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WarehouseServiceFacadeProject.DTOs;
using System.Configuration;

namespace WarehouseServiceFacadeProject.Tests
{
    //Is there a point in testing a Facade? Aren't tests supposed to not depend on databases services etc? Therefore, we will be testing the "mock" facade
    //that retrieves data completly differently? All we are testing there is the mock.

    [TestClass]
    public class WarehouseFacadeTests
    {
        [TestMethod]
        public void TestWarhouseConnection()
        {

            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["WarehouseServiceURL"];

            //Act
            WarehouseServiceFacade facade1 = new WarehouseServiceFacade(badURL1);
            WarehouseServiceFacade facade2 = new WarehouseServiceFacade(badURL2);
            WarehouseServiceFacade facade3 = new WarehouseServiceFacade(badURL3);
            WarehouseServiceFacade facade4 = new WarehouseServiceFacade(badURL4);
            WarehouseServiceFacade facade5 = new WarehouseServiceFacade(goodURL1);

            var products1 = facade1.getProducts();
            var products2 = facade2.getProducts();
            var products3 = facade3.getProducts();
            var products4 = facade4.getProducts();
            var products5 = facade5.getProducts();

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);

            Assert.IsNull(products1);
            Assert.IsNull(products2);
            Assert.IsNull(products3);
            Assert.IsNull(products4);
            //Assert.IsNotNull(products5); //Requires service to be up and running. Should it be removed due to dependancy.
        }
    }
}
