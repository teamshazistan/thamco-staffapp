﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using CustomerProfileServiceFacadeProject;

namespace CustoProfileServiceFacadeProject.Tests
{
    [TestClass]
    public class CustomerFacade
    {
        [TestMethod]
        public void TestCustomerProfileConnection()
        {
            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["CustomerProfileServiceURL"];

            //Act
            CustomerProfileServiceFacade facade1 = new CustomerProfileServiceFacade(badURL1);
            CustomerProfileServiceFacade facade2 = new CustomerProfileServiceFacade(badURL2);
            CustomerProfileServiceFacade facade3 = new CustomerProfileServiceFacade(badURL3);
            CustomerProfileServiceFacade facade4 = new CustomerProfileServiceFacade(badURL4);
            CustomerProfileServiceFacade facade5 = new CustomerProfileServiceFacade(goodURL1);

            var customers1 = facade1.getCustomers();
            var customers2 = facade2.getCustomers();
            var customers3 = facade3.getCustomers();
            var customers4 = facade4.getCustomers();
            var customers5 = facade5.getCustomers();

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);

            Assert.IsNull(customers1);
            Assert.IsNull(customers2);
            Assert.IsNull(customers3);
            Assert.IsNull(customers4);
            //Assert.IsNotNull(customers5); //Requires service to be up and running. Should it be removed due to dependancy.
        }
    }
}
