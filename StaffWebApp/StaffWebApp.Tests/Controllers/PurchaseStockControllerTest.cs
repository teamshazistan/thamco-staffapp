﻿using CreditCardServiceFacadeProject;
using StaffWebApp.Controllers;
using StaffWebApp.ViewModels.ThirdPartyStock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ThirdPartyFacades;
using WarehouseServiceFacadeProject;
using NUnit.Framework;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class PurchaseStockControllerTest
    {
        [TestCase]
        public void PurchaseStockControllerIndexTest()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();
            ICreditCardServiceFacade ccFacade = new CreditCardServiceFacade_test();
            IDodgyDealersFacade ddFacade = new DodgyDealersFacade_test();
            IUnderCutterFacade ucFacade = new UnderCutterFacade_test();

            PurchaseStockController controller = new PurchaseStockController(whFacade, ccFacade, ucFacade, ddFacade, null);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;
            var result2 = (controller.Index(tpService: "UnderCutters") as ViewResult).ViewData.Model;
            var result3 = (controller.Index(tpService: null, ean: "5 102310 300410") as ViewResult).ViewData.Model;
            var result4 = (controller.Index(tpService: null, ean: null) as ViewResult).ViewData.Model;
            var result5 = (controller.Index(ean: "TEST1") as ViewResult).ViewData.Model;
            var result6 = (controller.Index(tpService: "TESTTP") as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result).Count() == 0);

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result2);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result2).Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result3);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result3).Count() == 0); //Returns 0 as the application requires the user to select the third party service.

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result4);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result4).Count() == 0);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result5);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result5).Count() == 0);

            Assert.IsNotNull(result6);
            Assert.IsInstanceOf(typeof(List<ThirdPartyStockViewModel>), result6);
            Assert.IsTrue(((List<ThirdPartyStockViewModel>)result6).Count() == 0);
        }

        [TestCase]
        public void PurchaseStockControllerDetailsTest()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();
            ICreditCardServiceFacade ccFacade = new CreditCardServiceFacade_test();
            IDodgyDealersFacade ddFacade = new DodgyDealersFacade_test();
            IUnderCutterFacade ucFacade = new UnderCutterFacade_test();

            PurchaseStockController controller = new PurchaseStockController(whFacade, ccFacade, ucFacade, ddFacade, null);

            // Act
            var result = controller.Details(id: null, tpService: null);
            var result2 = controller.Details(id: null, tpService: "UnderCutters");
            var result3 = (controller.Details(id: 1, tpService: "UnderCutters") as ViewResult).ViewData.Model;
            var result4 = controller.Details(id: 1, tpService: "FakeService");
            var result5 = (controller.Details(id: 2, tpService: "DodgyDealers") as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(ThirdPartyStockViewModel), result3);
            Assert.IsTrue(((ThirdPartyStockViewModel)result3).Provider == "UnderCutters");
            Assert.IsTrue(((ThirdPartyStockViewModel)result3).ProductID == 1);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result4);
            Assert.IsTrue(((HttpStatusCodeResult)result4).StatusCode == 404); //404 = NotFound

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(ThirdPartyStockViewModel), result5);
            Assert.IsTrue(((ThirdPartyStockViewModel)result5).Provider == "DodgyDealers");
            Assert.IsTrue(((ThirdPartyStockViewModel)result5).ProductID == 2);
        }

        [TestCase]
        public void PurchaseStockControllerPlaceOrderTest()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();
            ICreditCardServiceFacade ccFacade = new CreditCardServiceFacade_test();
            IDodgyDealersFacade ddFacade = new DodgyDealersFacade_test();
            IUnderCutterFacade ucFacade = new UnderCutterFacade_test();

            PurchaseStockController controller = new PurchaseStockController(whFacade, ccFacade, ucFacade, ddFacade, null);

            // Act
            var result = controller.PlaceOrder(id: null, tpService: null);
            var result2 = controller.PlaceOrder(id: null, tpService: "UnderCutters");
            var result3 = (controller.PlaceOrder(id: 1, tpService: "UnderCutters") as ViewResult).ViewData.Model;
            var result4 = controller.PlaceOrder(id: 1, tpService: "FakeService");
            var result5 = (controller.PlaceOrder(id: 2, tpService: "DodgyDealers") as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result3);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).Seller == "UnderCutters");
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).ProductID == 1);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result4);
            Assert.IsTrue(((HttpStatusCodeResult)result4).StatusCode == 404); //404 = NotFound

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result5);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result5).Seller == "DodgyDealers");
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result5).ProductID == 2);
        }

        [TestCase]
        public void PurchaseStockControllerPlaceOrderTestPOST()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();
            ICreditCardServiceFacade ccFacade = new CreditCardServiceFacade_test();
            IDodgyDealersFacade ddFacade = new DodgyDealersFacade_test();
            IUnderCutterFacade ucFacade = new UnderCutterFacade_test();

            PurchaseStockController controller = new PurchaseStockController(whFacade, ccFacade, ucFacade, ddFacade, null);
            ThirdPartyOrderStockViewModel test1 = new ThirdPartyOrderStockViewModel()
            {
                ProductID = 1,
                SelectedCreditCardNumber = "-802938376",
                Quantity = 12,
                Seller = "UnderCutters"
            };

            ThirdPartyOrderStockViewModel test2 = new ThirdPartyOrderStockViewModel()
            {
                ProductID = 2,
                SelectedCreditCardNumber = "-802938376",
                Quantity = 2,
                Seller = "BadSeller"
            };

            ThirdPartyOrderStockViewModel test3 = new ThirdPartyOrderStockViewModel()
            {
                ProductID = 2,
                SelectedCreditCardNumber = "BadCard",
                Quantity = 2,
                Seller = "UnderCutters"
            };

            ThirdPartyOrderStockViewModel test4 = new ThirdPartyOrderStockViewModel();

            // Act
            var result = controller.PlaceOrder(test1);
            var result2 = (controller.PlaceOrder(test2) as ViewResult).ViewData.Model;
            var result3 = (controller.PlaceOrder(test3) as ViewResult).ViewData.Model;
            var result4 = (controller.PlaceOrder(test4) as ViewResult).ViewData.Model;
            var result5 = (controller.PlaceOrder(null) as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result2);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result2).Seller == test2.Seller);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result2).Quantity == test2.Quantity);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result2).SelectedCreditCardNumber == test2.SelectedCreditCardNumber);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result2).ProductID == test2.ProductID);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result3);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).Seller == test3.Seller);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).Quantity == test3.Quantity);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).SelectedCreditCardNumber == test3.SelectedCreditCardNumber);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result3).ProductID == test3.ProductID);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result4);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result4).Seller == null);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result4).SelectedCreditCardNumber == null);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(ThirdPartyOrderStockViewModel), result5);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result5).Seller == null);
            Assert.IsTrue(((ThirdPartyOrderStockViewModel)result5).SelectedCreditCardNumber == null);
        }
    }
}