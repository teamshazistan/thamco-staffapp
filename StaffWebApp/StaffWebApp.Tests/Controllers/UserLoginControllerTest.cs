﻿using StaffWebApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StaffProfileServiceFacadeProject;
using System.Web.Mvc;
using StaffWebApp.ViewModels.Account;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class UserLoginControllerTest
    {
        [TestCase]
        public void UserLoginControllerIndexTest()
        {
            // Arrange
            IStaffProfileServiceFacade spFacade = new StaffProfileServiceFacade_test();
            IUserLoginWrapper usLogin = new UserLoginWrapper_test();

            UserLoginController controller = new UserLoginController(spFacade, usLogin);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNull(result);
        }

        [TestCase]
        public void UserLoginControllerLoginTest()
        {
            // Arrange
            IStaffProfileServiceFacade spFacade = new StaffProfileServiceFacade_test();
            IUserLoginWrapper usLogin = new UserLoginWrapper_test();

            UserLoginController controller = new UserLoginController(spFacade, usLogin);

            // Act
            var result = (controller.Login() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNull(result);
        }

        [TestCase]
        public void UserLoginControllerLoginTestPOST()
        {
            // Arrange
            IStaffProfileServiceFacade spFacade = new StaffProfileServiceFacade_test();
            IUserLoginWrapper usLogin = new UserLoginWrapper_test();

            UserLoginController controller = new UserLoginController(spFacade, usLogin);

            AccountLogin test1 = new AccountLogin() { Username = "darien@dev.com", Password = "DarPassword" };
            AccountLogin test2 = new AccountLogin() { Username = "darien@dev.com", Password = "darpassword" };
            AccountLogin test3 = new AccountLogin() { Username = "DArien@dev.com", Password = "DarPassword" };
            AccountLogin test4 = new AccountLogin() { Username = "BadUnserName", Password = "DarPassword" };
            AccountLogin test5 = new AccountLogin() { Username = "", Password = "" };
            AccountLogin test6 = new AccountLogin() { Username = null, Password = null };

            // Act
            var result = controller.Login(test1);
            var result2 = (controller.Login(test2) as ViewResult).ViewData.Model;
            var result3 = (controller.Login(test3) as ViewResult).ViewData.Model;
            var result4 = (controller.Login(test4) as ViewResult).ViewData.Model;
            var result5 = (controller.Login(test5) as ViewResult).ViewData.Model;
            var result6 = (controller.Login(test6) as ViewResult).ViewData.Model;
            var result7 = (controller.Login(null) as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(AccountLogin), result2);
            Assert.IsTrue(((AccountLogin)result2).Username == test2.Username);
            Assert.IsTrue(((AccountLogin)result2).Password == test2.Password);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(AccountLogin), result3);
            Assert.IsTrue(((AccountLogin)result3).Username == test3.Username);
            Assert.IsTrue(((AccountLogin)result3).Password == test3.Password);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(AccountLogin), result4);
            Assert.IsTrue(((AccountLogin)result4).Username == test4.Username);
            Assert.IsTrue(((AccountLogin)result4).Password == test4.Password);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(AccountLogin), result5);
            Assert.IsTrue(((AccountLogin)result5).Username == test5.Username);
            Assert.IsTrue(((AccountLogin)result5).Password == test5.Password);

            Assert.IsNotNull(result6);
            Assert.IsInstanceOf(typeof(AccountLogin), result6);
            Assert.IsTrue(((AccountLogin)result6).Username == test6.Username);
            Assert.IsTrue(((AccountLogin)result6).Password == test6.Password);

            Assert.IsNotNull(result7);
            Assert.IsInstanceOf(typeof(AccountLogin), result7);
            Assert.IsTrue(((AccountLogin)result7).Username == null);
            Assert.IsTrue(((AccountLogin)result7).Password == null);
        }

        [TestCase]
        public void UserLoginControllerLogoutTest()
        {
            // Arrange
            IStaffProfileServiceFacade spFacade = new StaffProfileServiceFacade_test();
            IUserLoginWrapper usLogin = new UserLoginWrapper_test();

            UserLoginController controller = new UserLoginController(spFacade, usLogin);

            // Act
            var result = controller.Logout();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");
        }
    }
}