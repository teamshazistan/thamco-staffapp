﻿using MessageServiceFacadeProject;
using StaffWebApp.Controllers;
using StaffWebApp.ViewModels.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class MessageCentreControllerTest
    {
        [TestCase]
        public void MessageCentreControllerIndexTest()
        {
            // Arrange
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();
            MessageCentreController controller = new MessageCentreController(msFacade);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<MessageViewModel>), result);
            Assert.IsTrue(((List<MessageViewModel>)result).Count() > 0);
        }

        [TestCase]
        public void MessageCentreControllerCreateTest()
        {
            // Arrange
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();
            MessageCentreController controller = new MessageCentreController(msFacade);

            // Act
            var result = (controller.Create() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNull(result);
        }

        [TestCase]
        public void MessageCentreControllerCreateTestPOST()
        {
            // Arrange
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();
            MessageCentreController controller = new MessageCentreController(msFacade);

            NewMessageViewModel test1 = new NewMessageViewModel() { Contents = "This is a test message", RecipientString = "Recipient1;Recipient2" };
            NewMessageViewModel test2 = new NewMessageViewModel() { Contents = "This is a test message", RecipientString = "" };
            NewMessageViewModel test3 = new NewMessageViewModel();

            // Act
            var result = controller.Create(test1);
            var result2 = controller.Create(test2);
            var result3 = (controller.Create(test3) as ViewResult).ViewData.Model;
            var result4 = (controller.Create(null) as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result2);
            Assert.IsTrue(((RedirectToRouteResult)result2).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(NewMessageViewModel), result3);
            Assert.IsTrue(((NewMessageViewModel)result3).RecipientString == null);
            Assert.IsTrue(((NewMessageViewModel)result3).Contents == null);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(NewMessageViewModel), result4);
            Assert.IsTrue(((NewMessageViewModel)result4).RecipientString == null);
            Assert.IsTrue(((NewMessageViewModel)result4).Contents == null);
        }
    }
}