﻿using CustomerProfileServiceFacadeProject;
using MessageServiceFacadeProject;
using PurchasingServiceFacadeProject;
using StaffWebApp.Controllers;
using StaffWebApp.ViewModels.Customer;
using StaffWebApp.ViewModels.Customer.Orders;
using StaffWebApp.ViewModels.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class CustomerControllerTest
    {
        [TestCase]
        public void CustomerControllerIndexTest()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;
            var result2 = (controller.Index(customerNo: "P44_54", firstName: "Susan") as ViewResult).ViewData.Model;
            var result3 = (controller.Index(customerNo: "P86_44", firstName: "Goodwin-Madden") as ViewResult).ViewData.Model;
            var result4 = (controller.Index(customerNo: "10") as ViewResult).ViewData.Model;
            var result5 = (controller.Index(customerNo: null) as ViewResult).ViewData.Model;
            var result6 = (controller.Index(lastName: "Henderson") as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result).Count() > 0);

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result2);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result2).Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result3);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result3).Count() == 0);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result4);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result4).Count() == 0);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result5);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result5).Count() == 0);

            Assert.IsNotNull(result6);
            Assert.IsInstanceOf(typeof(List<CustomerProfileViewModel>), result6);
            Assert.IsTrue(((List<CustomerProfileViewModel>)result6).Count() > 0);
        }

        [TestCase]
        public void CustomerControllerProfileTest()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);

            // Act
            var result = controller.Profile(customerNo: "");
            var result2 = (controller.Profile(customerNo: "P44_54") as ViewResult).ViewData.Model;
            var result3 = (controller.Profile(customerNo: "P86_44") as ViewResult).ViewData.Model;
            var result4 = controller.Profile(customerNo: "10");
            var result5 = controller.Profile(customerNo: null);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(CustomerProfileViewModel), result2);
            Assert.IsTrue(((CustomerProfileViewModel)result2).CustomerNo == "P44_54");

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(CustomerProfileViewModel), result3);
            Assert.IsTrue(((CustomerProfileViewModel)result3).CustomerNo == "P86_44");

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result4);
            Assert.IsTrue(((HttpNotFoundResult)result4).StatusCode == 404); //404 = NotFound

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result5);
            Assert.IsTrue(((HttpStatusCodeResult)result5).StatusCode == 400); //400 = BadRequest
        }

        [TestCase]
        public void CustomerControllerEditPermsTest()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);

            // Act
            var result = controller.EditPerms(customerNo: "");
            var result2 = controller.EditPerms(customerNo: null);
            var result3 = (controller.EditPerms(customerNo: "P86_44") as ViewResult).ViewData.Model;
            var result4 = controller.EditPerms(customerNo: "TEST");

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(CustomerProfileViewModel), result3);
            Assert.IsTrue(((CustomerProfileViewModel)result3).CustomerNo == "P86_44");

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result4);
            Assert.IsTrue(((HttpNotFoundResult)result4).StatusCode == 404); //404 = NotFound
        }

        [TestCase]
        public void CustomerControllerEditPermsTestPOST()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);
            CustomerProfileViewModel test1 = new CustomerProfileViewModel() { CustomerApproved = true, CustomerCanPurchase = false, CustomerFName = "Steve", CustomerLName = "Wilson", CustomerDOB = new DateTime(1970, 2, 17).ToString(), CustomerGender = "MALE", CustomerNo = "P81_12" };
            CustomerProfileViewModel test2 = new CustomerProfileViewModel() { CustomerApproved = true, CustomerCanPurchase = true, CustomerFName = "Amy", CustomerLName = "Henderson", CustomerDOB = new DateTime(2000, 8, 28).ToString(), CustomerGender = "FEMALE", CustomerNo = "P55_11" };
            CustomerProfileViewModel test3 = new CustomerProfileViewModel();

            // Act
            var result = controller.EditPerms(customerVM: test1);
            var result2 = controller.EditPerms(customerVM: test2);
            var result3 = controller.EditPerms(customerVM: test3);
            var result4 = (controller.EditPerms(customerVM: null) as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result2);
            Assert.IsTrue(((RedirectToRouteResult)result2).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result3);
            Assert.IsTrue(((RedirectToRouteResult)result3).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNull(result4);
        }

        [TestCase]
        public void CustomerControllerOrderHistoryTest()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);

            // Act
            var result = controller.OrderHistory(customerNo: "");
            var result2 = (controller.OrderHistory(customerNo: "P44_54") as ViewResult).ViewData.Model;
            var result3 = (controller.OrderHistory(customerNo: "P86_44") as ViewResult).ViewData.Model;
            var result4 = controller.OrderHistory(customerNo: "10");
            var result5 = controller.OrderHistory(customerNo: null);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(List<CustomerOrdersViewModel>), result2);
            Assert.IsTrue(((List<CustomerOrdersViewModel>)result2).Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(List<CustomerOrdersViewModel>), result3);
            Assert.IsTrue(((List<CustomerOrdersViewModel>)result3).Count() > 0);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result4);
            Assert.IsTrue(((HttpNotFoundResult)result4).StatusCode == 404); //404 = NotFound

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result5);
            Assert.IsTrue(((HttpStatusCodeResult)result5).StatusCode == 400); //400 = BadRequest
        }

        [TestCase]
        public void CustomerControllerMessageHistoryTest()
        {
            // Arrange
            ICustomerProfileServiceFacade cpFacade = new CustomerProfileServiceFacade_test();
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();
            IMessageServiceFacade msFacade = new MessageServiceFacade_test();

            CustomerController controller = new CustomerController(cpFacade, psFacade, msFacade);

            // Act
            var result = controller.MessageHistory(customerNo: "");
            var result2 = (controller.MessageHistory(customerNo: "P44_54") as ViewResult).ViewData.Model;
            var result3 = (controller.MessageHistory(customerNo: "P86_44") as ViewResult).ViewData.Model;
            var result4 = (controller.MessageHistory(customerNo: "10") as ViewResult).ViewData.Model;
            var result5 = controller.MessageHistory(customerNo: null);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(List<MessageViewModel>), result2);
            Assert.IsTrue(((List<MessageViewModel>)result2).Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(List<MessageViewModel>), result3);
            Assert.IsTrue(((List<MessageViewModel>)result3).Count() > 0);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(List<MessageViewModel>), result4);
            Assert.IsTrue(((List<MessageViewModel>)result4).Count() == 1); //Would originally return not found, changed to display page anyway. Provides a single MessageViewModel that stores the customer number to allow user to go back a page.

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result5);
            Assert.IsTrue(((HttpStatusCodeResult)result5).StatusCode == 400); //400 = BadRequest
        }
    }
}