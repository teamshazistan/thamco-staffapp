﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarehouseServiceFacadeProject;
using NUnit.Framework;
using StaffWebApp.ViewModels.Stock;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class StockControllerTest
    {
        [TestCase]
        public void StockControllerIndexTest()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();

            StockController controller = new StockController(whFacade);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<WarehouseStockViewModel>), result);
            Assert.IsTrue(((List<WarehouseStockViewModel>)result).Count() > 0);
        }

        [TestCase]
        public void StockControllerEditTest()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();

            StockController controller = new StockController(whFacade);

            // Act
            var result = controller.Edit();
            var result2 = controller.Edit(ean: null);
            var result3 = controller.Edit(ean: "");
            var result4 = (controller.Edit(ean: "0123 4567 8902") as ViewResult).ViewData.Model;
            var result5 = controller.Edit(ean: "NotAProductEan");

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result3);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(WarehouseStockViewModel), result4);
            Assert.IsTrue(((WarehouseStockViewModel)result4).Ean == "0123 4567 8902");

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(HttpNotFoundResult), result5);
            Assert.IsTrue(((HttpNotFoundResult)result5).StatusCode == 404); //404 = NotFound

        }

        [TestCase]
        public void StockControllerEditTestPOST()
        {
            // Arrange
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();

            StockController controller = new StockController(whFacade);

            WarehouseStockViewModel test1 = new WarehouseStockViewModel() { Ean = "0123 4567 8902", Price = 10, RestockWarningLevel = 10 };
            WarehouseStockViewModel test2 = new WarehouseStockViewModel() { Ean = "NotAProductEan", Price = 10, RestockWarningLevel = 10 };
            WarehouseStockViewModel test3 = new WarehouseStockViewModel();

            // Act
            var result = controller.Edit(stockVM: test1);
            var result2 = (controller.Edit(stockVM: test2) as ViewResult).ViewData.Model;
            var result3 = (controller.Edit(stockVM: test3) as ViewResult).ViewData.Model;
            var result4 = (controller.Edit(stockVM: null) as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(WarehouseStockViewModel), result2);
            Assert.IsTrue(((WarehouseStockViewModel)result2).Ean == test2.Ean);
            Assert.IsTrue(((WarehouseStockViewModel)result2).Price == test2.Price);
            Assert.IsTrue(((WarehouseStockViewModel)result2).RestockWarningLevel == test2.RestockWarningLevel);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(WarehouseStockViewModel), result3);
            Assert.IsTrue(((WarehouseStockViewModel)result3).Ean == test3.Ean);
            Assert.IsTrue(((WarehouseStockViewModel)result3).Price == test3.Price);
            Assert.IsTrue(((WarehouseStockViewModel)result3).RestockWarningLevel == test3.RestockWarningLevel);

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(WarehouseStockViewModel), result4);
            Assert.IsTrue(((WarehouseStockViewModel)result4).Ean == null);
            Assert.IsTrue(((WarehouseStockViewModel)result4).Price == 0);
            Assert.IsTrue(((WarehouseStockViewModel)result4).RestockWarningLevel == 0);
        }

        [TestCase]
        public void StockControllerPriceHistoryTest()
        {
            IWarehouseServiceFacade whFacade = new WarehouseServiceFacade_test();

            StockController controller = new StockController(whFacade);

            // Act
            var result = controller.PriceHistory();
            var result2 = controller.PriceHistory(ean: null);
            var result3 = controller.PriceHistory(ean: "");
            var result4 = (controller.PriceHistory(ean: "0123 4567 8902") as ViewResult).ViewData.Model;
            var result5 = (controller.PriceHistory(ean: "NotAProductEan") as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result);
            Assert.IsTrue(((HttpStatusCodeResult)result).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result3);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(List<PriceHistoryViewModel>), result4);
            Assert.IsTrue(((List<PriceHistoryViewModel>)result4).Count() > 0);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(List<PriceHistoryViewModel>), result5);
            Assert.IsTrue(((List<PriceHistoryViewModel>)result5).Count() == 0);
        }
    }
}