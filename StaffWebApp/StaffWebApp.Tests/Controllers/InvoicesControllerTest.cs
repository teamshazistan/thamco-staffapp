﻿using PurchasingServiceFacadeProject;
using StaffWebApp.Controllers;
using StaffWebApp.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;

namespace StaffWebApp.Controllers.Tests
{
    [TestFixture]
    public class InvoicesControllerTest
    {

        [TestCase]
        public void InvoicesControllerIndexTest()
        {
            // Arrange
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();

            InvoicesController controller = new InvoicesController(psFacade);

            // Act
            var result = (controller.Index() as ViewResult).ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<PendingInvoicesViewModel>), result);
            Assert.IsTrue(((List<PendingInvoicesViewModel>)result).Count() > 0);
        }

        [TestCase]
        public void InvoicesControllerSendInvoicesTest()
        {
            // Arrange
            IPurchasingServiceFacade psFacade = new PurchasingServiceFacade_test();

            InvoicesController controller = new InvoicesController(psFacade);

            // Act
            var result = controller.SendInvoices("I00000002");
            var result2 = controller.SendInvoices("");
            var result3 = controller.SendInvoices(null);
            var result4 = controller.SendInvoices("TEST");

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result);
            Assert.IsTrue(((RedirectToRouteResult)result).RouteValues.Values.FirstOrDefault().ToString() == "Index");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result2);
            Assert.IsTrue(((HttpStatusCodeResult)result2).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(HttpStatusCodeResult), result3);
            Assert.IsTrue(((HttpStatusCodeResult)result3).StatusCode == 400); //400 = BadRequest

            Assert.IsNotNull(result4);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), result4);
            Assert.IsTrue(((RedirectToRouteResult)result4).RouteValues.Values.FirstOrDefault().ToString() == "Index");
        }
    }
}