﻿using StaffProfileServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace StaffProfileServiceFacadeProject
{
    #region Interface
    public interface IStaffProfileServiceFacade
    {
        SP_StaffProfile getStaffProfileFromLogin(SP_StaffLoginDTO loginDTO);
        IEnumerable<SP_StaffPermsDTO> getStaffPermsFromID(string ID);
    }
    #endregion


    #region StaffProfile Facade
    public class StaffProfileServiceFacade : IStaffProfileServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        #region Constructors
        public StaffProfileServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["StaffProfileServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public StaffProfileServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }
        #endregion

        #region Get
        public SP_StaffProfile getStaffProfileFromLogin(SP_StaffLoginDTO loginDTO)
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync("api/Login", loginDTO).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<SP_StaffProfile>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public IEnumerable<SP_StaffPermsDTO> getStaffPermsFromID(string ID)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/GetStaffAuthById?id={0}", ID);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<SP_StaffPermsDTO>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion
    }
    #endregion

    #region StaffProfile Test Facade
    public class StaffProfileServiceFacade_test : IStaffProfileServiceFacade
    {
        private List<SP_StaffProfile> testStaffProfiles;
        private List<SP_StaffPerms> testPermissions;

        public StaffProfileServiceFacade_test()
        {
            testPermissions = new List<SP_StaffPerms>()
            {
                new SP_StaffPerms() { UniqueId = "S00000001", Permissions =  new List<string>() { "CanEditProducts", "CanViewStockLevel", "CanViewProductPriceHistory", "CanPurchaseFromThirdParty", "CanSendMessages", "CanSendPendingInvoices", "CanViewCustomerMessageHistory", "CanViewCustomerOrderHistory", "CanViewCustomerProfile", "CanEditCustomerPerms"} },
                new SP_StaffPerms() { UniqueId = "S00000002", Permissions =  new List<string>() { "CanEditProducts", "CanViewProductPriceHistory", "CanPurchaseFromThirdParty", "CanViewCustomerMessageHistory", "CanViewCustomerOrderHistory", "CanViewCustomerProfile", "CanEditCustomerPerms"} } ,
                new SP_StaffPerms() { UniqueId = "S00000003", Permissions =  new List<string>() { "CanEditProducts", "CanViewStockLevel", "CanViewProductPriceHistory", "CanSendMessages", "CanSendPendingInvoices", "CanViewCustomerMessageHistory"} } ,
            };

            testStaffProfiles = new List<SP_StaffProfile>()
            {
                new SP_StaffProfile() { UniqueId = "S00000001", Username = "darien@dev.com", PasswordHash = SHA1.Encode("DarPassword"), FirstName = "Darien", LastName = "Goodwin-Madden" },
                new SP_StaffProfile() { UniqueId = "S00000002", Username = "mark@dev.com", PasswordHash = SHA1.Encode("MarkPassword"), FirstName = "Mark", LastName = "Leonard" },
                new SP_StaffProfile() { UniqueId = "S00000003", Username = "ross@dev.com", PasswordHash = SHA1.Encode("RossPassword"), FirstName = "Ross", LastName = "Williams" },
                new SP_StaffProfile() { UniqueId = "S00000004", Username = "josh@dev.com", PasswordHash = SHA1.Encode("JoshPassword"), FirstName = "Josh", LastName = "Percival" },
                new SP_StaffProfile() { UniqueId = "S00000005", Username = "sean@dev.com", PasswordHash = SHA1.Encode("SeanPassword"), FirstName = "Sean", LastName = "Johnson" },
            };

        }

        public IEnumerable<SP_StaffPermsDTO> getStaffPermsFromID(string ID)
        {
            var perms = testPermissions.Where(x => x.UniqueId == ID).FirstOrDefault();
            if(perms != null)
            {
                return perms.Permissions.Select(x => new SP_StaffPermsDTO() { authorisation = x }).ToList();
            }
            return null;
        }

        public SP_StaffProfile getStaffProfileFromLogin(SP_StaffLoginDTO loginDTO)
        {
            return testStaffProfiles.Where(x => x.Username == loginDTO.Username && x.PasswordHash == loginDTO.Password).FirstOrDefault();
        }
    }
    #endregion
}
