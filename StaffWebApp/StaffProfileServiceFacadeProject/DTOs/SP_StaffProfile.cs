﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffProfileServiceFacadeProject.DTOs
{
    public class SP_StaffProfile
    {
        public string UniqueId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Username { get; set; }
        public string PasswordHash { get; set; }
    }
}
