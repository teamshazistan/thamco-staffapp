﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffProfileServiceFacadeProject.DTOs
{
    public class SP_StaffPerms
    {
        public string UniqueId { get; set; }
        public List<string> Permissions { get; set; }
    }
}
