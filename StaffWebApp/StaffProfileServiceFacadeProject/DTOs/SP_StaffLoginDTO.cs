﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffProfileServiceFacadeProject.DTOs
{
    public class SP_StaffLoginDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
