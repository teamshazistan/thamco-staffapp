﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ThirdPartyFacades.DTOs;
using ThirdPartyFacades.DTOs.UnderCutters;

namespace ThirdPartyFacades
{
    public interface IUnderCutterFacade
    {
        IEnumerable<UC_Category> getCategories();
        IEnumerable<UC_Brand> getBrands();
        IEnumerable<UC_Product> getProducts();
        UC_Product getProductByID(int ID);
        bool placeOrder(Combined_Order newOrder);
    }

    #region UnderCutterFacade
    /// <summary>
    /// The UnderCutter service Facade. Calls the appropriate APIs when requested by the user.
    /// </summary>
    public class UnderCutterFacade : IUnderCutterFacade
    {
        private HttpClient client;

        public UnderCutterFacade()
        {
            client = new HttpClient();
            client.BaseAddress = new System.Uri(ConfigurationManager.AppSettings["UnderCutterServiceURL"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public IEnumerable<UC_Category> getCategories()
        {
            HttpResponseMessage response = client.GetAsync("api/category").Result;
            if (response.IsSuccessStatusCode)
                return response.Content.ReadAsAsync<IEnumerable<UC_Category>>().Result;
            else
                Debug.WriteLine("Index received a bad response from the web service.");

            return null;
        }

        public IEnumerable<UC_Brand> getBrands()
        {
            HttpResponseMessage response = client.GetAsync("api/brand").Result;
            if (response.IsSuccessStatusCode)
                return response.Content.ReadAsAsync<IEnumerable<UC_Brand>>().Result;
            else
                Debug.WriteLine("Index received a bad response from the web service.");

            return null;
        }

        public IEnumerable<UC_Product> getProducts()
        {
            HttpResponseMessage response = client.GetAsync("api/product").Result;
            if (response.IsSuccessStatusCode)
                return response.Content.ReadAsAsync<IEnumerable<UC_Product>>().Result;
            else
                Debug.WriteLine("Index received a bad response from the web service.");

            return null;
        }

        public UC_Product getProductByID(int ID)
        {
            string apiString = String.Format("api/Product/{0}", ID);
            HttpResponseMessage response = client.GetAsync(apiString).Result;
            if (response.IsSuccessStatusCode)
                return response.Content.ReadAsAsync<UC_Product>().Result;
            else
                Debug.WriteLine("Index received a bad response from the web service.");

            return null;
        }

        public bool placeOrder(Combined_Order newOrder)
        {
            try
            {
                var reponse = client.PostAsJsonAsync("api/Order", newOrder).Result;
                if (reponse.IsSuccessStatusCode)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception thrown: " + ex.Message);
                return false;
            }
        }
    }
    #endregion

    #region UnderCutterFacade Test
    public class UnderCutterFacade_test : IUnderCutterFacade
    {
        private List<UC_Brand> testBrands;
        private List<UC_Category> testCategories;
        private List<UC_Product> testProducts;

        public UnderCutterFacade_test()
        {
            testBrands = new List<UC_Brand>()
            {
                new UC_Brand () { Id = 1, Name = "iStuff-R-Us", AvailableProductCount = 3 },
                new UC_Brand () { Id = 2, Name = "Damp Squib", AvailableProductCount = 4 },
                new UC_Brand () { Id = 3, Name = "Soggy Sponge", AvailableProductCount = 7 },
                new UC_Brand () { Id = 4, Name = "RoboBits and That", AvailableProductCount = 3 },
            };

            testCategories = new List<UC_Category>()
            {
                new UC_Category() { AvailableProductCount = 5, Description = "Imitation Davison Stores screen protectors for your phone or tablet.", Id = 1, Name = "Screen Protectors" },
                new UC_Category() { AvailableProductCount = 4, Description = "UnderCutters Stores pride ourselves on our poor range of covers for your mobile device at premium prices.  If you're lucky your phone or tablet will be protected from any dents, scratches and scuffs.", Id = 2, Name = "Covers" },
                new UC_Category() { AvailableProductCount = 2, Description = "Browse our wide narrow range of cases for phones and tablets that will help you to keep your mobile device protected.", Id = 3, Name = "Case" },
                new UC_Category() { AvailableProductCount = 6, Description = "We stock a huge small range of phone and tablet accessories that we cannot be bothered to classify in other categories.", Id = 4, Name = "Accessories" }
            };

            testProducts = new List<UC_Product>()
            {
                new UC_Product() { BrandId = 1, BrandName = "iStuff-R-Us", CategoryId = 1, CategoryName = "Screen Protectors", Description = "For his or her sensory pleasure. Fits few known smartphones.", Ean = "5 102310 300410", Id = 1, InStock = true, Name = "Rippled Screen Protector", Price = 8.24 },
                new UC_Product() { BrandId = 3, BrandName = "Soggy Sponge", CategoryId = 2, CategoryName = "Covers", Description = "Poor quality fake faux leather cover loose enough to fit any mobile device.", Ean = "5 102310 100101", Id = 2, InStock = true, Name = "Wrap It and Hope Cover", Price = 5.8 }
            };
        }


        public IEnumerable<UC_Brand> getBrands()
        {
            return testBrands;
        }

        public IEnumerable<UC_Category> getCategories()
        {
            return testCategories;
        }

        public UC_Product getProductByID(int ID)
        {
            return testProducts.Where(x => x.Id == ID).FirstOrDefault();
        }

        public IEnumerable<UC_Product> getProducts()
        {
            return testProducts;
        }

        public bool placeOrder(Combined_Order newOrder)
        {
            return true;
        }
    }
    #endregion
}
