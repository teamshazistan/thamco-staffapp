﻿using ThirdPartyFacades.BazzasBazaarService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThirdPartyFacades;
using ThirdPartyFacades.DTOs;
using ThirdPartyFacades.DTOs.UnderCutters;
using ThirdPartyFacades.DTOs.DodgyDealers;
using AutoMapper;
using System.Diagnostics;
using System.Net.Http;

namespace ThirdPartyFacades
{
    public static class ThirdPartyFacadeHandler
    {
        public static IDodgyDealersFacade ddFacade;
        private static string ddNameString = "DodgyDealers";

        public static IUnderCutterFacade ucFacade;
        private static string ucNameString = "UnderCutters";

        public static IStore bbFacade;
        private static string bbNameString = "BazzasBazaar";

        private static IMapper iMapper;
        private static AutoMapper.MapperConfiguration mapperConfig;
        static ThirdPartyFacadeHandler()
        {
            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Product, Combined_Product>()/*.ForMember(destination => destination.ExpectedRestock, opts => opts.MapFrom(source => source.ExpectedRestock == null ? "" : source.ExpectedRestock.ToString() ))*/;
                cfg.CreateMap<UC_Product, Combined_Product>().ForMember(destination => destination.PriceForOne, opts => opts.MapFrom(source => source.Price));
                cfg.CreateMap<DD_Product, Combined_Product>().ForMember(destination => destination.PriceForOne, opts => opts.MapFrom(source => source.Price));
            });

            iMapper = mapperConfig.CreateMapper();
        }

        public static List<string> returnViewBagList()
        {
            return new List<string>()
            { ddNameString, ucNameString, bbNameString };
        }

        #region APICalls

        #region Brands
        #endregion

        #region Categories
        #endregion

        #region Products
        public static IEnumerable<Combined_Product> getProducts(string tpService)
        {
            if (tpService == ddNameString)
            {
                var returnedProducts = ddFacade.getProducts();
                if (returnedProducts != null && returnedProducts.Count() > 0)
                {
                    var combinedProducts = returnedProducts.Select(x => iMapper.Map<DD_Product, Combined_Product>(x)).ToList();
                    if (combinedProducts != null)
                    {
                        combinedProducts.ForEach(x => x.Provider = ddNameString);
                        return combinedProducts;
                    }
                }
                Debug.WriteLine("Unable to retrieve products from DodgyDealers");
            }
            if (tpService == ucNameString)
            {
                var returnedProducts = ucFacade.getProducts();
                if (returnedProducts != null && returnedProducts.Count() > 0)
                {
                    var combinedProducts = returnedProducts.Select(x => iMapper.Map<UC_Product, Combined_Product>(x)).ToList();
                    if (combinedProducts != null)
                    {
                        combinedProducts.ForEach(x => x.Provider = ucNameString);
                        return combinedProducts;
                    }
                }
                Debug.WriteLine("Unable to retrieve products from UnderCutters");
            }
            if (tpService == bbNameString)
            {
                var categories = bbFacade.GetAllCategories();
                if (categories != null && categories.Count() > 0)
                {
                    List<Combined_Product> returnedProducts = new List<Combined_Product>();
                    foreach (var cat in categories)
                    {
                        var bbProducts = bbFacade.GetFilteredProducts(cat.Id, cat.Name, null, null);
                        if(bbProducts != null && bbProducts.Count() > 0)
                            returnedProducts.AddRange(bbProducts.Select(x => iMapper.Map<Product, Combined_Product>(x)).ToList());
                    }
                    if (returnedProducts.Count() > 0)
                    {
                        returnedProducts.ForEach(x => x.Provider = bbNameString);
                        return returnedProducts;
                    }
                }
                Debug.WriteLine("Unable to retrieve products from BazzasBazaar");
            }
            return new List<Combined_Product>();
        }


        public static Combined_Product getProductsByID(int ID, string tpService)
        {
            if (tpService == ddNameString)
            {
                var returnedProduct = ddFacade.getProductByID(ID);
                if (returnedProduct != null)
                {
                    var combinedProduct = iMapper.Map<DD_Product, Combined_Product>(returnedProduct);
                    combinedProduct.Provider = ddNameString;
                    return combinedProduct;
                }
                Debug.WriteLine(String.Format("Unable to retrieve product id:{0} from DodgyDealers", ID));
            }

            if (tpService == ucNameString)
            {
                var returnedProduct = ucFacade.getProductByID(ID);
                if (returnedProduct != null)
                {
                    var combinedProduct = iMapper.Map<UC_Product, Combined_Product>(returnedProduct);
                    combinedProduct.Provider = ucNameString;
                    return combinedProduct;
                }
                Debug.WriteLine(String.Format("Unable to retrieve product id:{0} from UnderCutters", ID));
            }

            if (tpService == bbNameString)
            {
                var returnedProduct = bbFacade.GetProductById(ID);
                if (returnedProduct != null)
                {
                    var combinedProduct = iMapper.Map<Product, Combined_Product>(returnedProduct);
                    combinedProduct.Provider = bbNameString;
                    return combinedProduct;
                }
                Debug.WriteLine(String.Format("Unable to retrieve product id:{0} from BazzasBazaar", ID));
            }

            return null;
        }
        #endregion

        #region Orders

        public static bool placeOrder(Combined_Order newOrder, string tpService)
        {
            if (tpService == ddNameString)
                return ddFacade.placeOrder(newOrder);

            if (tpService == ucNameString)
                return ucFacade.placeOrder(newOrder);

            if (tpService == bbNameString)
            {
                //Try catch is included here as expection handling is handled in the other facades in the placeOrder method.
                try
                {
                    var order = bbFacade.CreateOrder(newOrder.AccountName, newOrder.CardNumber, newOrder.ProductId, newOrder.Quantity);
                    if (order != null)
                        return true;
                }
                catch (Exception ex) { }
            }
            return false;
        }
        #endregion

        #endregion
    }
}
