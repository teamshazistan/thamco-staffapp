﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdPartyFacades.DTOs.DodgyDealers
{
    public class DD_Brand
    {
        public int AvailableProductCount { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
