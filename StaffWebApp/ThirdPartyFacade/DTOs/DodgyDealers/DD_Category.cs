﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdPartyFacades.DTOs.DodgyDealers
{
    public class DD_Category
    {
        public int AvailableProductCount { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
