﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace MessageServiceFacadeProject.Tests
{
    [TestClass]
    public class MessageFacade
    {
        [TestMethod]
        public void TestMessageConnection()
        {
            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["MessageServiceURL"];

            //Act
            MessageServiceFacade facade1 = new MessageServiceFacade(badURL1);
            MessageServiceFacade facade2 = new MessageServiceFacade(badURL2);
            MessageServiceFacade facade3 = new MessageServiceFacade(badURL3);
            MessageServiceFacade facade4 = new MessageServiceFacade(badURL4);
            MessageServiceFacade facade5 = new MessageServiceFacade(goodURL1);

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);
        }
    }
}
