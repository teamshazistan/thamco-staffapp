﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    public class WH_Brand
    {
        public int AvailableProductCount { get; set; }
        //public int BrandID { get; set; }
        public string Name { get; set; }
    }
}
