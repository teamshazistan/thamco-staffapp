﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    public class WH_Product
    {
        public int ProductID { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ProductDescription { get; set; }
        public string Ean { get; set; }
        public string ExpectedRestock { get; set; }
        public int StockLevel { get; set; }
        public int RestockWarningLevel { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
    }
}
