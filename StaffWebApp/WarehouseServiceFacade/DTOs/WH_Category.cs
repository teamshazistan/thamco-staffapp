﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    public class WH_Category
    {
        public int AvailableProductCount { get; set; }
        public string Description { get; set; }
        //public int CategoryID { get; set; }
        public string Name { get; set; }
    }
}
