﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    public class WH_ProductPriceUpdate
    {
        public string Ean { get; set; }
        public int RestockWarningLevel { get; set; }
        public double Price { get; set; }
    }
}
