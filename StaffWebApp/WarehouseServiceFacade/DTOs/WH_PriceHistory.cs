﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    public class WH_PriceHistory
    {
        public string Ean { get; set; }
        public string productName { get; set; }
        public double Price { get; set; }
        public DateTime DateChanged { get; set; }
    }
}
