﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarehouseServiceFacadeProject.DTOs
{
    /// <summary>
    /// This DTO is passed to the warehouse service. It is required that additional information such as the product name/description is included
    /// to allow the warehouse service to add new products to its database.
    /// </summary>
    public class WH_ProductStockUpdate
    {
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
        public string Ean { get; set; }
        public int StockToAdd { get; set; }
        public double Price { get; set; }
    }
}
