﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarehouseServiceFacadeProject.DTOs;

namespace WarehouseServiceFacadeProject
{
    public interface IWarehouseServiceFacade
    {
        IEnumerable<WH_Category> getCategories();
        IEnumerable<WH_Brand> getBrands();
        IEnumerable<WH_Product> getProducts();
        IEnumerable<WH_PriceHistory> getPriceHistoryByEAN(string EAN);
        WH_Product getProductByEAN(string EAN);
        bool addProductStock(List<WH_ProductStockUpdate> productStockUpdate);
        bool editProduct(WH_ProductPriceUpdate productToUpdate);
    }

    public class WarehouseServiceFacade : IWarehouseServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }
        public WarehouseServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["WarehouseServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public WarehouseServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch(Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        #region Brands
        public IEnumerable<WH_Brand> getBrands()
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync("api/brand").Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<WH_Brand>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion

        #region Categories
        public IEnumerable<WH_Category> getCategories()
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync("api/category").Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<WH_Category>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion

        #region Products
        public IEnumerable<WH_Product> getProducts()
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync("api/Products").Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<WH_Product>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public WH_Product getProductByEAN(string EAN)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/Product?Ean={0}", EAN);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<WH_Product>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public IEnumerable<WH_PriceHistory> getPriceHistoryByEAN(string EAN)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/GetPriceHistoryForProduct?Ean={0}", EAN);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<WH_PriceHistory>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        //No HTTP response.
        public bool addProductStock(List<WH_ProductStockUpdate> productStockUpdate)
        {
            if (IsConnected)
            {
                try
                {
                    var reponse = client.PostAsJsonAsync("api/AddStock", productStockUpdate).Result;
                    if (reponse.IsSuccessStatusCode)
                        return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return false;
        }

        public bool editProduct(WH_ProductPriceUpdate productToUpdate)
        {
            if (IsConnected)
            {
                try
                {
                    //Temp as Ross has for some reason made it so his API only accepts a collection of these objects. Only editing one at a time.
                    List<WH_ProductPriceUpdate> productList = new List<WH_ProductPriceUpdate>();
                    productList.Add(productToUpdate);
                    var reponse = client.PostAsJsonAsync("api/UpdateProduct", productList).Result;
                    if (reponse.IsSuccessStatusCode)
                        return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return false;
        }
        #endregion
    }

    /// <summary>
    /// Test class of the WarehouseServiceFacade. Contains test data used for testing purposes.
    /// </summary>
    public class WarehouseServiceFacade_test : IWarehouseServiceFacade
    {
        private List<WH_Brand> testBrandData;
        private List<WH_Category> testCatData;
        private List<WH_Product> testProductData;
        private List<WH_PriceHistory> testPriceHistoryData;

        public WarehouseServiceFacade_test()
        {
            testBrandData = new List<WH_Brand>()
            {
                new WH_Brand { /*BrandID = 0,*/ Name = "Soggy Sponge",  AvailableProductCount = 2 },
                new WH_Brand { /*BrandID = 1,*/ Name = "Damp Squib", AvailableProductCount = 3  },
                new WH_Brand { /*BrandID = 2,*/ Name = "iStuff-R-Us", AvailableProductCount = 6 }
            };

            testCatData = new List<WH_Category>()
            {
                new WH_Category { /*CategoryID = 0,*/ Name = "Covers", Description = "Davison Stores pride ourselves on our poor range of covers for your mobile device at premium prices.  If you're lukcy your phone or tablet will be protected from any dents, scratches and scuffs.", AvailableProductCount = 5 },
                new WH_Category { /*CategoryID = 1,*/ Name = "Case", Description = "Browse our wide range of cases for phones and tablets that will help you to keep your mobile device protected from the elements.", AvailableProductCount = 2 },
                new WH_Category { /*CategoryID = 2,*/ Name = "Accessories", Description = "We stock a small range of phone and tablet accessories, including car holders, sports armbands, stylus pens and very little else.", AvailableProductCount = 4 },
                new WH_Category { /*CategoryID = 3,*/ Name = "Screen Protectors", Description = "Exclusive Davison Stores screen protectors for your phone or tablet.", AvailableProductCount = 1 }
            };

            testProductData = new List<WH_Product>()
            {
                new WH_Product { ProductID = 1, Ean = "0123 4567 8901", ProductDescription = "Poor quality fake faux leather cover loose enough to fit any mobile device.", ProductName = "Wrap It and Hope Cover", Price = 5.99, StockLevel = 1, BrandName = testBrandData[0].Name, CategoryName = testCatData[0].Name, RestockWarningLevel = 5 },
                new WH_Product { ProductID = 2, Ean = "0123 4567 8902", ProductDescription = "Purchase you favourite chocolate and use the provided heating element to melt it into the perfect cover for your mobile device.", ProductName = "Chocolate Cover", Price = 10.97, StockLevel = 0, BrandName = testBrandData[2].Name, CategoryName = testCatData[3].Name, RestockWarningLevel = 2  },
                new WH_Product { ProductID = 3, Ean = "0123 4567 8903", ProductDescription = "Lamely adapted used and dirty teatowel.  Guaranteed fewer than two holes.", ProductName = "Cloth Cover", Price = 3.01, StockLevel = 6, BrandName = testBrandData[1].Name, CategoryName = testCatData[1].Name, RestockWarningLevel = 1  },
                new WH_Product { ProductID = 4, Ean = "0123 4567 8904", ProductDescription = "Especially toughen and harden sponge entirely encases your device to prevent any interaction.", ProductName = "Harden Sponge Case", Price = 9.99, StockLevel = 2, BrandName = testBrandData[2].Name, CategoryName = testCatData[2].Name  },
                new WH_Product { ProductID = 5, Ean = "0123 4567 8905", ProductDescription = "Place your device within the water-tight container, fill with water and enjoy the cushioned protection from bumps and bangs.", ProductName = "Water Bath Case", Price = 20.0, StockLevel = 3, BrandName = testBrandData[1].Name, CategoryName = testCatData[0].Name, RestockWarningLevel = 10  },
                new WH_Product { ProductID = 6, Ean = "0123 4567 8906", ProductDescription = "Keep you smartphone handsfree with this large assembly that attaches to your rear window wiper (Hatchbacks only).", ProductName = "Smartphone Car Holder", Price = 110.01, StockLevel = 8, BrandName = testBrandData[0].Name, CategoryName = testCatData[1].Name, RestockWarningLevel = 2 },
                new WH_Product { ProductID = 7, Ean = "0123 4567 8907", ProductDescription = "Keep your device on your arm with this general purpose sticky tape.", ProductName = "Sticky Tape Sport Armband", Price = 2.99, StockLevel = 23, BrandName = testBrandData[2].Name, CategoryName = testCatData[0].Name, RestockWarningLevel = 3  },
                new WH_Product { ProductID = 8, Ean = "0123 4567 8908", ProductDescription = "Stengthen HB pencils guaranteed to leave a mark.", ProductName = "Real Pencil Stylus", Price = 0.99, StockLevel = 5, BrandName = testBrandData[0].Name, CategoryName = testCatData[2].Name, RestockWarningLevel = 3  },
                new WH_Product { ProductID = 9, Ean = "0123 4567 8909", ProductDescription = "Coat your mobile device screen in a scratch resistant, opaque film.", ProductName = "Spray Paint Screen Protector", Price = 4.99, StockLevel = 1, BrandName = testBrandData[0].Name, CategoryName = testCatData[3].Name, RestockWarningLevel = 4  },
                new WH_Product { ProductID = 10, Ean = "0123 4567 8910", ProductDescription = "For his or her sensory pleasure. Fits few known smartphones.", ProductName = "Rippled Screen Protector", Price = 7.99, StockLevel = 5, BrandName = testBrandData[0].Name, CategoryName = testCatData[1].Name, RestockWarningLevel = 5   },
                new WH_Product { ProductID = 11, Ean = "0123 4567 8911", ProductDescription = "For an odour than lingers on your device.", ProductName = "Fish Scented Screen Protector", Price = 2.88, StockLevel = 0, BrandName = testBrandData[2].Name, CategoryName = testCatData[2].Name, RestockWarningLevel = 1  },
                new WH_Product { ProductID = 12, Ean = "0123 4567 8912", ProductDescription = "Guaranteed not to conduct electical charge from your fingers.", ProductName = "Non-conductive Screen Protector", Price = 10.0, StockLevel = 10, BrandName = testBrandData[1].Name, CategoryName = testCatData[2].Name, RestockWarningLevel = 15   },
            };

            testPriceHistoryData = new List<WH_PriceHistory>()
            {
                new WH_PriceHistory { Ean = "0123 4567 8901", DateChanged = new DateTime(2017, 01, 02, 12, 3, 10 ), Price = 10.99, productName = "Wrap It and Hope Cover" },
                new WH_PriceHistory { Ean = "0123 4567 8901", DateChanged = new DateTime(2017, 02, 02, 12, 3, 10 ), Price = 11.99, productName = "Wrap It and Hope Cover" },
                new WH_PriceHistory { Ean = "0123 4567 8901", DateChanged = new DateTime(2017, 03, 02, 12, 3, 10 ), Price = 5.99, productName = "Wrap It and Hope Cover" },
                new WH_PriceHistory { Ean = "0123 4567 8902", DateChanged = new DateTime(2017, 01, 02, 12, 3, 10 ), Price = 10.99, productName = "Chocolate Cover" },
                new WH_PriceHistory { Ean = "0123 4567 8902", DateChanged = new DateTime(2017, 02, 02, 12, 3, 10 ), Price = 11.99, productName = "Chocolate Cover" },
                new WH_PriceHistory { Ean = "0123 4567 8903", DateChanged = new DateTime(2017, 03, 02, 12, 3, 10 ), Price = 5.99, productName = "Cloth Cover" },
            };
        }

        public IEnumerable<WH_Brand> getBrands()
        {
            return testBrandData;
        }

        public IEnumerable<WH_Category> getCategories()
        {
            return testCatData;
        }

        public IEnumerable<WH_Product> getProducts()
        {
            return testProductData;
        }

        public WH_Product getProductByEAN(string EAN)
        {
            return testProductData.Where(x => x.Ean == EAN).FirstOrDefault();
        }

        public IEnumerable<WH_PriceHistory> getPriceHistoryByEAN(string EAN)
        {
            return testPriceHistoryData.Where(x => x.Ean == EAN);
        }

        public bool addProductStock(List<WH_ProductStockUpdate> productStockUpdate)
        {
            try
            {
                foreach (var product in productStockUpdate)
                {
                    if (product.StockToAdd > 0)
                    {
                        var storedProduct = testProductData.Where(x => x.Ean == product.Ean).FirstOrDefault();
                        if (storedProduct != null)
                            storedProduct.StockLevel += product.StockToAdd;
                        else
                        {
                            int newId = testProductData.Max(x => x.ProductID) + 1;
                            testProductData.Add(new WH_Product()
                            {
                                ProductID = newId,
                                BrandName = product.BrandName,
                                CategoryName = product.CategoryName,
                                ProductDescription = product.ProductDescription,
                                Ean = product.Ean,
                                ProductName = product.ProductName,
                                StockLevel = product.StockToAdd,
                            });
                        }
                    }
                }
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool editProduct(WH_ProductPriceUpdate productToUpdate)
        {
            var toEdit = testProductData.Where(x => x.Ean == productToUpdate.Ean).FirstOrDefault();
            if (toEdit != null)
            {
                toEdit.Price = productToUpdate.Price;
                toEdit.RestockWarningLevel = productToUpdate.RestockWarningLevel;
                return true;
            }
            return false;
        }
    }
}
