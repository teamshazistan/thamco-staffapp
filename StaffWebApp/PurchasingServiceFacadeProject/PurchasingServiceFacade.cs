﻿using PurchasingServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingServiceFacadeProject
{
    #region Interface
    public interface IPurchasingServiceFacade
    {
        IEnumerable<PC_Orders> getOrdersFromCustomerNo(string customerNo);
        IEnumerable<PC_Invoices> getPendingInvoices();
        IEnumerable<PC_Invoices> getInvoices(string customerNo, string invoiceNo, string orderNo);
        PC_Invoices sendInvoice(string invoiceNo);
    }
    #endregion

    #region Purchasing Facade
    public class PurchasingServiceFacade : IPurchasingServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        #region Constructors
        public PurchasingServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["PurchasingServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public PurchasingServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }
        #endregion

        #region Orders
        public IEnumerable<PC_Orders> getOrdersFromCustomerNo(string customerNo)
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/Orders?customerNo={0}", customerNo);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<PC_Orders>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion

        #region Invoices
        public IEnumerable<PC_Invoices> getPendingInvoices()
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/PendingInvoices");
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<PC_Invoices>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public IEnumerable<PC_Invoices> getInvoices(string customerNo = "", string invoiceNo = "", string orderNo = "")
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/Invoices?customerNo={0}&invoiceNo={1}&orderNo={2}", customerNo, invoiceNo, orderNo);
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<PC_Invoices>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        public PC_Invoices sendInvoice(string invoiceNo)
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync("api/SendInvoice", new PC_InvoiceStatusUpdateDTO() { InvoiceNo = invoiceNo, SentToCustomer = true }).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<PC_Invoices>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }
        #endregion
    }
    #endregion

    #region Purchasing Test Facade
    public class PurchasingServiceFacade_test : IPurchasingServiceFacade
    {
        private List<PC_OrderedItems> testOrderedItems;
        private List<PC_OrderAddress> testOrderAddress;
        private List<PC_Orders> testOrders;
        private List<PC_Invoices> testInvoices;

        public PurchasingServiceFacade_test()
        {
            testOrderAddress = new List<PC_OrderAddress>()
            {
                new PC_OrderAddress() { Address1 = "TestAddress1", Address2 = "TestAddress2", Address3 = "TestAddress3", City = "TestCity", Country = "TestCountry", County = "TestCounty", PostCode = "TestPostCode" },
                new PC_OrderAddress() { Address1 = "TestAddress1", Address2 = "TestAddress2", Address3 = "TestAddress3", City = "TestCity", Country = "TestCountry", County = "", PostCode = "TestPostCode" },
                new PC_OrderAddress() { Address1 = "TestAddress1A", Address2 = "TestAddress2A", Address3 = "TestAddress3A", City = "TestCityA", Country = "TestCountryA", County = "TestCountyA", PostCode = "TestPostCodeA" },
                new PC_OrderAddress() { Address1 = "TestAddress1B", Address2 = "TestAddress2B", Address3 = "TestAddress3B", City = "TestCityB", Country = "TestCountryB", County = "TestCountyB", PostCode = "TestPostCodeB" },
            };

            testOrderedItems = new List<PC_OrderedItems>()
            {
                new PC_OrderedItems() { ItemEAN = "TestCRNA", ItemDesc = "TestDescA", ItemName = "TestNameA", ItemQuanity = 10, ItemUnitPrice = 10.29 },
                new PC_OrderedItems() { ItemEAN = "TestCRNB", ItemDesc = "TestDescB", ItemName = "TestNameB", ItemQuanity = 1, ItemUnitPrice = 1.29 },
                new PC_OrderedItems() { ItemEAN = "TestCRNC", ItemDesc = "TestDescC", ItemName = "TestNameC", ItemQuanity = 5, ItemUnitPrice = 5.99 },
                new PC_OrderedItems() { ItemEAN = "TestCRND", ItemDesc = "TestDescD", ItemName = "TestNameD", ItemQuanity = 2, ItemUnitPrice = 2.00 },
            };

            testOrders = new List<PC_Orders>()
            {
                new PC_Orders() { CustomerNo = "P44_54", BillingAddress = testOrderAddress[0], ShippingAddress = testOrderAddress[0], OrderedItems = new List<PC_OrderedItems>() { testOrderedItems[0], testOrderedItems[1]} },
                new PC_Orders() { CustomerNo = "P86_44", BillingAddress = testOrderAddress[1], ShippingAddress = testOrderAddress[2], OrderedItems = new List<PC_OrderedItems>() { testOrderedItems[2], testOrderedItems[3]} }
            };

            testInvoices = new List<PC_Invoices>()
            {
                new PC_Invoices() { InvoiceNo = "I00000001", OrderList = new List<PC_Orders>() { testOrders[0] }, SentToCustomer = true, InvoiceDate = new DateTime(2017, 01, 02, 10, 20, 0) },
                new PC_Invoices() { InvoiceNo = "I00000002", OrderList = new List<PC_Orders>() { testOrders[1] }, SentToCustomer = false, InvoiceDate = new DateTime(2017, 01, 20, 10, 20, 0) },
            };
        }

        #region Orders
        public IEnumerable<PC_Orders> getOrdersFromCustomerNo(string customerNo)
        {
            return testOrders.Where(x => x.CustomerNo == customerNo);
        }
        #endregion

        #region Invoices
        public IEnumerable<PC_Invoices> getInvoices(string customerNo = "", string invoiceNo = "", string orderNo = "")
        {
            var invoices = testInvoices.Where(x => x.SentToCustomer);

            if (invoiceNo != "")
                invoices = invoices.Where(x => x.InvoiceNo == invoiceNo);
            if (orderNo != "")
                invoices = invoices.Where(x => x.OrderList.Any(y => y.PurchaseOrderNo == orderNo));
            if (customerNo != "")
                invoices = invoices.Where(x => x.OrderList.Any(y => y.CustomerNo == customerNo));

            return invoices;
        }

        public IEnumerable<PC_Invoices> getPendingInvoices()
        {
            return testInvoices.Where(x => !x.SentToCustomer);
        }

        public PC_Invoices sendInvoice(string invoiceNo)
        {
            var invoice = testInvoices.Where(x => x.InvoiceNo == invoiceNo).FirstOrDefault();
            if (invoice != null)
                invoice.SentToCustomer = true;
            return invoice;
        }
        #endregion
    }
    #endregion
}
