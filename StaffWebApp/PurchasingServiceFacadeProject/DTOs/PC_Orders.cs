﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingServiceFacadeProject.DTOs
{
    public class PC_Orders
    {
        //The purchase order number of the customer order.
        public string PurchaseOrderNo { get; set; }

        //The customer number of the customer who made the order.
        public string CustomerNo { get; set; }

        //The description of the order status.
        public string OrderStatusDescription { get; set; }

        //A list of all items ordered in the current order.
        public IEnumerable<PC_OrderedItems> OrderedItems { get; set; }

        //The billing address object.
        public PC_OrderAddress BillingAddress { get; set; }

        //The shipping address object.
        public PC_OrderAddress ShippingAddress { get; set; }
    }
}
