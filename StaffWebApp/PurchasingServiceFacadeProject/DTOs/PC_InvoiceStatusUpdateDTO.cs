﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingServiceFacadeProject.DTOs
{
    public class PC_InvoiceStatusUpdateDTO
    {
        public string InvoiceNo { get; set; }
        public bool SentToCustomer { get; set; }
    }
}
