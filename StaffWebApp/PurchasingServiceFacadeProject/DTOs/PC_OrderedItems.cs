﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingServiceFacadeProject.DTOs
{
    public class PC_OrderedItems
    {
        //The EAN of the item being ordered
        public string ItemEAN { get; set; }

        //The name of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemName { get; set; }

        //The description of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemDesc { get; set; }

        //The number of items ordered by the customer
        public int ItemQuanity { get; set; }

        //The price of a single unit of the ordered item (£)
        public double ItemUnitPrice { get; set; }
    }
}
