﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace CreditCardServiceFacadeProject.Tests
{
    [TestClass]
    public class CreditCardFacadeTests
    {
        [TestMethod]
        public void TestCreditCardConnection()
        {

            //Arrange
            string badURL1 = "http://fakeURL.com/";
            string badURL2 = "http://dfsdfdsd.com";
            string badURL3 = "Test";
            string badURL4 = "";
            string goodURL1 = ConfigurationManager.AppSettings["CreditCardServiceURL"];

            //Act
            CreditCardServiceFacade facade1 = new CreditCardServiceFacade(badURL1);
            CreditCardServiceFacade facade2 = new CreditCardServiceFacade(badURL2);
            CreditCardServiceFacade facade3 = new CreditCardServiceFacade(badURL3);
            CreditCardServiceFacade facade4 = new CreditCardServiceFacade(badURL4);
            CreditCardServiceFacade facade5 = new CreditCardServiceFacade(goodURL1);

            var products1 = facade1.getCreditCards();
            var products2 = facade2.getCreditCards();
            var products3 = facade3.getCreditCards();
            var products4 = facade4.getCreditCards();
            var products5 = facade5.getCreditCards();

            //Assert
            Assert.IsTrue(facade1.IsConnected);
            Assert.IsTrue(facade2.IsConnected);
            Assert.IsFalse(facade3.IsConnected);
            Assert.IsFalse(facade4.IsConnected);
            Assert.IsTrue(facade5.IsConnected);

            Assert.IsNull(products1);
            Assert.IsNull(products2);
            Assert.IsNull(products3);
            Assert.IsNull(products4);
            //Assert.IsNotNull(products5); //Requires service to be up and running. Should it be removed due to dependancy.
        }
    }
}
