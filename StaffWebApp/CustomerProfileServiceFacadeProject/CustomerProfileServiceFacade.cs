﻿using CustomerProfileServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CustomerProfileServiceFacadeProject
{
    #region Interface
    public interface ICustomerProfileServiceFacade
    {
        IEnumerable<CP_CustomerProfile> getCustomers();
        IEnumerable<CP_CustomerProfile> getCustomersFiltered(string customerNo, string fName, string lName);
        void editCustomer(CP_CustomerProfileUpdate editedCustomers);
    }
    #endregion


    #region CustomerProfile Facade
    public class CustomerProfileServiceFacade : ICustomerProfileServiceFacade
    {
        protected HttpClient client;
        public bool IsConnected { get; set; }

        #region Constructors
        public CustomerProfileServiceFacade()
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["CustomerProfileServiceURL"]);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }

        public CustomerProfileServiceFacade(string address)
        {
            client = new HttpClient();
            try
            {
                client.BaseAddress = new Uri(address);
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
            }
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
        }
        #endregion

        /// <summary>
        /// Returns a list of all active customers in the customer profile service.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CP_CustomerProfile> getCustomers()
        {
            if (IsConnected)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync("api/Customers").Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<CP_CustomerProfile>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a list of all active customers in the customer profile service filtered by their customer number, first name and or last name.
        /// </summary>
        /// <param name="customerNo">The entered customer number.</param>
        /// <param name="fName">The entered first name.</param>
        /// <param name="lName">The entered last name.</param>
        /// <returns></returns>
        public IEnumerable<CP_CustomerProfile> getCustomersFiltered(string customerNo = "", string fName = "", string lName = "")
        {
            if (IsConnected)
            {
                try
                {
                    string apiString = String.Format("api/Customers?customerNo={0}&fName={1}&lName={2}", customerNo, fName, lName); //Needs to be adjusted depending on how Josh wants to people to communicate with service.
                    HttpResponseMessage response = client.GetAsync(apiString).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<IEnumerable<CP_CustomerProfile>>().Result;
                    else
                        Debug.WriteLine("Index received a bad response from the web service.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// Sends a DTOs object to the customer profile service which contains a customers number and edited parameters.
        /// </summary>
        /// <param name="editedCustomers">A CP_CustomerProfileUpdate object. Contains updated data.</param>
        public void editCustomer(CP_CustomerProfileUpdate editedCustomers)
        {
            if (IsConnected)
            {
                try
                {
                    var reponse = client.PostAsJsonAsync("api/Customer/Update", editedCustomers).Result;
                    if (!reponse.IsSuccessStatusCode)
                        Debug.WriteLine("Unable to update customers.");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception thrown: " + ex.Message);
                }
            }
        }
    }
    #endregion


    #region CustomerProfile Test Facade
    public class CustomerProfileServiceFacade_test : ICustomerProfileServiceFacade
    {
        private IEnumerable<CP_CustomerProfile> testCustomerProfiles;

        public CustomerProfileServiceFacade_test()
        {
            testCustomerProfiles = new List<CP_CustomerProfile>()
            {
                new CP_CustomerProfile() { CustomerApproved = true, CustomerCanPurchase = true, CustomerFName = "Susan", CustomerLName = "Hammond", CustomerDOB = new DateTime(1990, 12, 2), CustomerGender = "FEMALE", CustomerNo = "P44_54" },
                new CP_CustomerProfile() { CustomerApproved = true, CustomerCanPurchase = false, CustomerFName = "Darien", CustomerLName = "Goodwin-Madden", CustomerDOB = new DateTime(1996, 7, 4), CustomerGender = "MALE", CustomerNo = "P86_44" },
                new CP_CustomerProfile() { CustomerApproved = false, CustomerCanPurchase = false, CustomerFName = "Steve", CustomerLName = "Wilson", CustomerDOB = new DateTime(1970, 2, 17), CustomerGender = "MALE", CustomerNo = "P81_12" },
                new CP_CustomerProfile() { CustomerApproved = false, CustomerCanPurchase = false, CustomerFName = "Amy", CustomerLName = "Henderson", CustomerDOB = new DateTime(2000, 8, 28), CustomerGender = "FEMALE", CustomerNo = "P55_11" }
            };

        }

        public IEnumerable<CP_CustomerProfile> getCustomers()
        {
            return testCustomerProfiles;
        }

        public IEnumerable<CP_CustomerProfile> getCustomersFiltered(string customerNo = "", string fName = "", string lName = "")
        {
            IEnumerable<CP_CustomerProfile> returedCustomers = testCustomerProfiles;

            if(customerNo != "")
                returedCustomers = returedCustomers.Where(x => x.CustomerNo == customerNo);
            if (fName != null && fName != "")
                returedCustomers = returedCustomers.Where(x => x.CustomerFName == fName);
            if (lName != null && lName != "")
                returedCustomers = returedCustomers.Where(x => x.CustomerLName == lName);

            return returedCustomers;
        }

        public void editCustomer(CP_CustomerProfileUpdate editedCustomers)
        {
            CP_CustomerProfile customerProfile = testCustomerProfiles.Where(x => x.CustomerNo == editedCustomers.CustomerNo).FirstOrDefault();
            if (customerProfile != null)
            {
                customerProfile.CustomerCanPurchase = editedCustomers.CustomerCanPurchase;
                customerProfile.CustomerApproved = editedCustomers.CustomerApproved;
            }
        }
    }
    #endregion
}
