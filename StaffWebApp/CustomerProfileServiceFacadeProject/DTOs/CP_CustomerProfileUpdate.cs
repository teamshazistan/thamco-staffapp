﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerProfileServiceFacadeProject.DTOs
{
    public class CP_CustomerProfileUpdate
    {
        public string CustomerNo { get; set; }
        public bool CustomerApproved { get; set; }
        public bool CustomerCanPurchase { get; set; }
    }
}
