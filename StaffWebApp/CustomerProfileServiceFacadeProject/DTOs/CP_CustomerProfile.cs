﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerProfileServiceFacadeProject.DTOs
{
    public class CP_CustomerProfile
    {
        //Will be modified when Josh provides more info on the returned structure of DTO. Temporary format.
        public string CustomerNo { get; set; }
        public string CustomerFName { get; set; }
        public string CustomerLName { get; set; }
        public DateTime CustomerDOB { get; set; }
        public string CustomerGender { get; set; }
        public bool CustomerApproved { get; set; }
        public bool CustomerCanPurchase { get; set; }
    }
}
