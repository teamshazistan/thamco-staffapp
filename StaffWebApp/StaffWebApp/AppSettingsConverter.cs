﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace StaffWebApp
{
    /// <summary>
    /// A class dedicated to retrieving configuartion values from the web.config file and converting the string to the appropriate data type.
    /// </summary>
    public static class AppSettingsConverter
    {
        public static int getIntAppSetting(string appSettingName, int defaultValue)
        {
            int tempInt = 0;
            if (Int32.TryParse(ConfigurationManager.AppSettings[appSettingName], out tempInt))
            {
                Debug.WriteLine(String.Format("Unable to parse '{0}' to an int.", appSettingName));
                return defaultValue;
            }
            return tempInt;
        }

        public static double getDoubleAppSetting(string appSettingName, double defaultValue)
        {
            double tempDouble = 0.0;
            if (Double.TryParse(ConfigurationManager.AppSettings[appSettingName], out tempDouble))
            {
                Debug.WriteLine(String.Format("Unable to parse '{0}' to a double.", appSettingName));
                return defaultValue;
            }
            return tempDouble;
        }

        public static bool getBooleanAppSetting(string appSettingName, bool defaultValue)
        {
            bool tempBool = false;
            if (Boolean.TryParse(ConfigurationManager.AppSettings[appSettingName], out tempBool))
            {
                Debug.WriteLine(String.Format("Unable to parse '{0}' to a boolean.", appSettingName));
                return defaultValue;
            }
            return tempBool;
        }
    }
}