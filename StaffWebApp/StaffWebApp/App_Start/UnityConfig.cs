using CreditCardServiceFacadeProject;
using CustomerProfileServiceFacadeProject;
using MessageServiceFacadeProject;
using PurchasingServiceFacadeProject;
using StaffProfileServiceFacadeProject;
using System.Configuration;
using System.Web.Mvc;
using ThirdPartyFacades;
using Unity;
using Unity.Mvc5;
using WarehouseServiceFacadeProject;

namespace StaffWebApp
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            bool debugBool = false;
            bool stagingBool = false;
            bool releaseBool = false;
            bool.TryParse(ConfigurationManager.AppSettings["DebugMode"], out debugBool);
            bool.TryParse(ConfigurationManager.AppSettings["StagingMode"], out stagingBool);
            bool.TryParse(ConfigurationManager.AppSettings["ReleaseMode"], out releaseBool);

            //Note: Staging and Release use the same services. If others set up staging URLs, passing the different mode URLs should occur.
            if (debugBool)
            {
                container.RegisterType<IWarehouseServiceFacade, WarehouseServiceFacade_test>();
                container.RegisterType<IMessageServiceFacade, MessageServiceFacade_test>();
                container.RegisterType<IStaffProfileServiceFacade, StaffProfileServiceFacade_test>();
                container.RegisterType<ICustomerProfileServiceFacade, CustomerProfileServiceFacade_test>();
                container.RegisterType<IPurchasingServiceFacade, PurchasingServiceFacade_test>();
                container.RegisterType<ICreditCardServiceFacade, CreditCardServiceFacade_test>();
                container.RegisterType<IDodgyDealersFacade, DodgyDealersFacade_test>();
                container.RegisterType<IUnderCutterFacade, UnderCutterFacade_test>();
                container.RegisterType<IUserLoginWrapper, UserLoginWrapper>();

                container.RegisterInstance<IWarehouseServiceFacade>(new WarehouseServiceFacade_test());
                container.RegisterInstance<IMessageServiceFacade>(new MessageServiceFacade_test());
                container.RegisterInstance<IStaffProfileServiceFacade>(new StaffProfileServiceFacade_test());
                container.RegisterInstance<ICustomerProfileServiceFacade>(new CustomerProfileServiceFacade_test());
                container.RegisterInstance<IPurchasingServiceFacade>(new PurchasingServiceFacade_test());
                container.RegisterInstance<ICreditCardServiceFacade>(new CreditCardServiceFacade_test());
                container.RegisterInstance<IDodgyDealersFacade>(new DodgyDealersFacade_test());
                container.RegisterInstance<IUnderCutterFacade>(new UnderCutterFacade_test());
                container.RegisterInstance<IUserLoginWrapper>(new UserLoginWrapper());
            }
            else if (stagingBool)
            {
                container.RegisterType<IWarehouseServiceFacade, WarehouseServiceFacade>();
                container.RegisterType<IMessageServiceFacade, MessageServiceFacade>();
                container.RegisterType<IStaffProfileServiceFacade, StaffProfileServiceFacade>();
                container.RegisterType<ICustomerProfileServiceFacade, CustomerProfileServiceFacade_test>(); //Service not up and running, remains test.
                container.RegisterType<IPurchasingServiceFacade, PurchasingServiceFacade>();
                container.RegisterType<ICreditCardServiceFacade, CreditCardServiceFacade>();
                container.RegisterType<IDodgyDealersFacade, DodgyDealersFacade>();
                container.RegisterType<IUnderCutterFacade, UnderCutterFacade>();
                container.RegisterType<IUserLoginWrapper, UserLoginWrapper>();

                //Provide URLs here.
                container.RegisterInstance<IWarehouseServiceFacade>(new WarehouseServiceFacade());
                container.RegisterInstance<IMessageServiceFacade>(new MessageServiceFacade());
                container.RegisterInstance<IStaffProfileServiceFacade>(new StaffProfileServiceFacade());
                container.RegisterInstance<ICustomerProfileServiceFacade>(new CustomerProfileServiceFacade_test()); //Service not up and running, remains test.
                container.RegisterInstance<IPurchasingServiceFacade>(new PurchasingServiceFacade());
                container.RegisterInstance<ICreditCardServiceFacade>(new CreditCardServiceFacade());
                container.RegisterInstance<IDodgyDealersFacade>(new DodgyDealersFacade());
                container.RegisterInstance<IUnderCutterFacade>(new UnderCutterFacade());
                container.RegisterInstance<IUserLoginWrapper>(new UserLoginWrapper());
            }
            else if(releaseBool)
            {
                container.RegisterType<IWarehouseServiceFacade, WarehouseServiceFacade>();
                container.RegisterType<IMessageServiceFacade, MessageServiceFacade>();
                container.RegisterType<IStaffProfileServiceFacade, StaffProfileServiceFacade>();
                container.RegisterType<ICustomerProfileServiceFacade, CustomerProfileServiceFacade_test>(); //Service not up and running, remains test.
                container.RegisterType<IPurchasingServiceFacade, PurchasingServiceFacade>();
                container.RegisterType<ICreditCardServiceFacade, CreditCardServiceFacade>();
                container.RegisterType<IDodgyDealersFacade, DodgyDealersFacade>();
                container.RegisterType<IUnderCutterFacade, UnderCutterFacade>();
                container.RegisterType<IUserLoginWrapper, UserLoginWrapper>();

                //Provide URLs here.
                container.RegisterInstance<IWarehouseServiceFacade>(new WarehouseServiceFacade());
                container.RegisterInstance<IMessageServiceFacade>(new MessageServiceFacade());
                container.RegisterInstance<IStaffProfileServiceFacade>(new StaffProfileServiceFacade());
                container.RegisterInstance<ICustomerProfileServiceFacade>(new CustomerProfileServiceFacade_test()); //Service not up and running, remains test.
                container.RegisterInstance<IPurchasingServiceFacade>(new PurchasingServiceFacade());
                container.RegisterInstance<ICreditCardServiceFacade>(new CreditCardServiceFacade());
                container.RegisterInstance<IDodgyDealersFacade>(new DodgyDealersFacade());
                container.RegisterInstance<IUnderCutterFacade>(new UnderCutterFacade());
                container.RegisterInstance<IUserLoginWrapper>(new UserLoginWrapper());
            }
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}