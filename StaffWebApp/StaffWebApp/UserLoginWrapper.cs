﻿using StaffProfileServiceFacadeProject;
using StaffProfileServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace StaffWebApp
{
    public interface IUserLoginWrapper
    {
        bool AuthenticateUser(string userName, bool rememberMe);
        bool SignOutUser();
        bool GenerateCookies(SP_StaffProfile staffProfile, IStaffProfileServiceFacade spFacade);
    }

    public class UserLoginWrapper : IUserLoginWrapper
    {
        public bool AuthenticateUser(string userName, bool rememberMe)
        {
            try
            {
                FormsAuthentication.SetAuthCookie(userName, rememberMe);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool GenerateCookies(SP_StaffProfile staffProfile, IStaffProfileServiceFacade spFacade)
        {
            try
            {
                List<string> roles = spFacade.getStaffPermsFromID(staffProfile.UniqueId).Select(x => x.authorisation).ToList();
                roles.Add("Staff");
                string ticketString = string.Join(",", roles.ToArray());

                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, staffProfile.Username, DateTime.Now, DateTime.Now.AddMinutes(20), false, ticketString, "/");
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                HttpContext.Current.Response.Cookies.Add(cookie);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SignOutUser()
        {
            try
            {
                FormsAuthentication.SignOut();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    public class UserLoginWrapper_test : IUserLoginWrapper
    {
        public bool AuthenticateUser(string userName, bool rememberMe)
        {
            return true;
        }

        public bool GenerateCookies(SP_StaffProfile staffProfile, IStaffProfileServiceFacade spFacade)
        {
            return true;
        }

        public bool SignOutUser()
        {
            return true;
        }
    }
}