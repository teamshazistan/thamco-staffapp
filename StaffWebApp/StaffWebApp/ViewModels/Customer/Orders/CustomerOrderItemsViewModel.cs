﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Customer.Orders
{
    public class CustomerOrderItemsViewModel
    {
        [Display(Name = "Item EAN")]
        public string ItemEAN { get; set; }

        [Display(Name = "Item Name")]
        //The name of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemName { get; set; }

        [Display(Name = "Item Description")]
        //The description of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemDesc { get; set; }

        [Display(Name = "Quantity")]
        //The number of items ordered by the customer
        public int ItemQuanity { get; set; }

        [Display(Name = "Unit Price")]
        //The price of a single unit of the ordered item (£)
        public double ItemUnitPrice { get; set; }
    }
}