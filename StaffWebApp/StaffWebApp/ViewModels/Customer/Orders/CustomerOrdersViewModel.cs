﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Customer.Orders
{
    public class CustomerOrdersViewModel
    {
        [Display(Name = "P.O.#")]
        //The purchase order number of the customer order.
        public string PurchaseOrderNo { get; set; }

        [Display(Name = "Customer #")]
        //The customer number of the customer who made the order.
        public string CustomerNo { get; set; }

        [Display(Name = "Order Status")]
        //The description of the order status.
        public string OrderStatusDescription { get; set; }

        //A list of all items ordered in the current order.
        public List<CustomerOrderItemsViewModel> OrderedItems { get; set; }
    }
}