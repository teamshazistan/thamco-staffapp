﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Customer
{
    public class CustomerProfileViewModel
    {
        //Will be modified when Josh provides more info on the returned structure of DTO. Temporary format. Currently exactly the same as the DTO.
        [Display(Name = "Customer Number")]
        public string CustomerNo { get; set; }
        [Display(Name = "Forename")]
        public string CustomerFName { get; set; }
        [Display(Name = "Surname")]
        public string CustomerLName { get; set; }
        [Display(Name = "DOB")]
        public string CustomerDOB { get; set; }
        [Display(Name = "Gender")]
        public string CustomerGender { get; set; }
        [Display(Name = "Approved?")]
        public bool CustomerApproved { get; set; }
        [Display(Name = "Can Purchase?")]
        public bool CustomerCanPurchase { get; set; }

        [Display(Name = "Name")]
        public string CustomerName
        {
            get
            {
                return String.Format("{0}.{1}", CustomerFName[0], CustomerLName);
            }
        }

    }
}