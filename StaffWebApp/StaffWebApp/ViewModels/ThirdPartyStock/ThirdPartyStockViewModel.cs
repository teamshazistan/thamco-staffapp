﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.ThirdPartyStock
{
    public class ThirdPartyStockViewModel
    {
        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public int ProductID { get; set; }
        [Display(Name = "Ean")]
        public string Ean { get; set; }
        [Display(Name = "Name", Description = "Name of the product")]
        public string Name { get; set; }
        [Display(Name = "Brand Name", Description = "Name of the product brand")]
        public string BrandName { get; set; }
        [Display(Name = "Category Name", Description = "Name of the product category")]
        public string CategoryName { get; set; }
        [Display(Name = "Description", Description = "A description of the product")]
        public string Description { get; set; }
        [Display(Name = "Price", Description = "The price of the product")]
        public double Price { get; set; }
        [Display(Name = "Provider", Description = "The name of the third party provider")]
        public string Provider { get; set; }
    }
}