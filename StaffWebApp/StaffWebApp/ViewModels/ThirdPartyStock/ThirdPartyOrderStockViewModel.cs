﻿using CreditCardServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StaffWebApp.ViewModels.ThirdPartyStock
{
    public class ThirdPartyOrderStockViewModel
    {
        public int ProductID { get; set; }
        [Display(Name = "Ean")]
        public string Ean { get; set; }
        [Display(Name = "Name", Description = "Name of the product")]
        public string Name { get; set; }
        [Display(Name = "Brand Name", Description = "Name of the product brand")]
        public string BrandName { get; set; }
        [Display(Name = "Category Name", Description = "Name of the product category")]
        public string CategoryName { get; set; }
        [Display(Name = "Description", Description = "A description of the product")]
        public string Description { get; set; }
        [Display(Name = "Price", Description = "The price of the a single unit of this product")]
        public double Price { get; set; }
        [Display(Name = "Price Per 10", Description = "The price of 10 units of this product")]
        public double? PricePer10 { get; set; }
        [Display(Name = "Seller", Description = "The seller of the product")]
        public string Seller { get; set; }

        [Display(Name = "Quantity", Description = "The quantity (units) of requested product")]
        public int Quantity { get; set; }
        [Display(Name = "Selected Credit Card", Description = "The selected credit card (hashed) to be used to purchase the current product")]
        public string SelectedCreditCardNumber { get; set; }
        [Display(Name = "Credit Card", Description = "The a list of credit cards to be used to purchase the current product")]
        public IEnumerable<CC_CreditCard> CreditCards { get; set; }

        public double TotalPrice
        {
            get
            {
                double _TotalPrice = 0;
                if ((PricePer10 == null || PricePer10 <= 0) || Quantity < 10)
                    _TotalPrice += Quantity * Price;
                else
                {
                    if (Quantity >= 10)
                    {
                        _TotalPrice += ((Quantity / 10) * (Double)PricePer10);
                        _TotalPrice += ((Quantity % 10) * Price);
                    }
                }

                return _TotalPrice;
            }
        }
    }
}