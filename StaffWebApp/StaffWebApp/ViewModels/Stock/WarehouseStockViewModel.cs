﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Stock
{
    public class WarehouseStockViewModel
    {
        public int ProductID { get; set; }
        [Display(Name = "Ean")]
        public string Ean { get; set; }
        [Display(Name = "Name", Description = "Name of the product")]
        public string ProductName { get; set; }
        [Display(Name = "Brand Name", Description = "Name of the product brand")]
        public string BrandName { get; set; }
        [Display(Name = "Category Name", Description = "Name of the product category")]
        public string CategoryName { get; set; }
        [Display(Name = "Description", Description = "A description of the product")]
        public string ProductDescription { get; set; }
        [Display(Name = "Price (£)", Description = "The price of the product")]
        public double Price { get; set; }
        [Display(Name = "Stock Level", Description = "The stock level of the product")]
        public int StockLevel { get; set; }
        [Display(Name = "Restock Warning Level")]
        public int RestockWarningLevel { get; set; }

        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public bool RestockWarningBool { get { return StockLevel < RestockWarningLevel; } }
    }
}