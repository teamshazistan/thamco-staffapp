﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Stock
{
    public class PriceHistoryViewModel
    {
        [Display(Name = "Ean", Description = "Ean of the product.")]
        public string Ean { get; set; }
        [Display(Name = "Name", Description = "Name of the product.")]
        public string productName { get; set; }
        [Display(Name = "Price (£)", Description = "Historical price of the product.")]
        public double Price { get; set; }
        [Display(Name = "Date Changed", Description = "Date when price of the product was changed.")]
        public DateTime DateChanged { get; set; }
    }
}