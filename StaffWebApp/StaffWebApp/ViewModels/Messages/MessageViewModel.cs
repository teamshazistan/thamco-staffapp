﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Messages
{
    public class MessageViewModel
    {
        [Display(Name = "Sender #")]
        public string SenderNo { get; set; }
        [Display(Name = "Message Contents")]
        public string Contents { get; set; }
        public string RecipientNo { get; set; }
    }
}