﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Messages
{
    public class NewMessageViewModel
    {
        [Display(Name = "Message Contents")]
        public string Contents { get; set; }
        [Display(Name = "Recipient(s)")]
        public string RecipientString { get; set; }
    }
}