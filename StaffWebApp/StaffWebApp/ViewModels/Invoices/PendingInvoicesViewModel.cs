﻿using StaffWebApp.ViewModels.Customer.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StaffWebApp.ViewModels.Invoices
{
    public class PendingInvoicesViewModel
    {
        //The invoice number of the invoice record.
        [Display(Name = "Invoice #")]
        public string InvoiceNo { get; set; }

        //The date and time when the invoice was generated.
        [Display(Name = "Invoice Date")]
        public DateTime InvoiceDate { get; set; }

        //The customer number of the customer assigned to the invoice.
        [Display(Name = "Customer #")]
        public string CustomerNo { get; set; }
    }
}