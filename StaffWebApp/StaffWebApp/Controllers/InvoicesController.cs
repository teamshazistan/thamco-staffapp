﻿using AutoMapper;
using PurchasingServiceFacadeProject;
using PurchasingServiceFacadeProject.DTOs;
using StaffWebApp.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace StaffWebApp.Controllers
{
    public class InvoicesController : Controller
    {
        private IPurchasingServiceFacade psFacade;
        private IMapper iMapper;
        private AutoMapper.MapperConfiguration mapperConfig;

        public InvoicesController(IPurchasingServiceFacade _psFacade)
        {
            psFacade = _psFacade;

            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PC_Invoices, PendingInvoicesViewModel>().ForMember(des => des.CustomerNo, opts => opts.MapFrom(src => src.OrderList.FirstOrDefault() != null ? src.OrderList.FirstOrDefault().CustomerNo : ""));
            });
            iMapper = mapperConfig.CreateMapper();
        }

        // GET: Invoices
        [Authorize]
        public ActionResult Index()
        {
            IEnumerable<PC_Invoices> pendingInvoices = psFacade.getPendingInvoices();
            List<PendingInvoicesViewModel> invoiceVM = new List<PendingInvoicesViewModel>();

            if (pendingInvoices != null)
                invoiceVM = pendingInvoices.Select(x => iMapper.Map<PC_Invoices, PendingInvoicesViewModel>(x)).ToList();
            return View(invoiceVM);
        }

        [Authorize(Roles = "CanSendPendingInvoices")]
        public ActionResult SendInvoices(string invoiceNo)
        {
            if (invoiceNo == null || invoiceNo == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            PC_Invoices invoice = psFacade.sendInvoice(invoiceNo);
            return RedirectToAction("Index"); //How do I add error here?
        }
    }
}