﻿using AutoMapper;
using MessageServiceFacadeProject;
using MessageServiceFacadeProject.DTOs;
using StaffWebApp.ViewModels.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StaffWebApp.Controllers
{
    public class MessageCentreController : Controller
    {
        private IMapper iMapper;
        private AutoMapper.MapperConfiguration mapperConfig;
        private IMessageServiceFacade msFacade;

        public MessageCentreController(IMessageServiceFacade _msFacade)
        {
            msFacade = _msFacade;

            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MS_Messages, MessageViewModel>();
            });
            iMapper = mapperConfig.CreateMapper();
        }


        // GET: MessageCentre
        [Authorize]
        public ActionResult Index()
        {
            IEnumerable<MS_Messages> messageList = msFacade.getMessagesFromSenderID(GetUserIdentity());
            if (messageList == null)
                return HttpNotFound();

            List<MessageViewModel> custMessageVM = messageList.Select(x => iMapper.Map<MS_Messages, MessageViewModel>(x)).ToList();

            return View(custMessageVM);
        }

        // GET: MessageCentre/Create
        [Authorize(Roles = "CanSendMessages")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: MessageCentre/Create
        [Authorize(Roles = "CanSendMessages")]
        [HttpPost]
        public ActionResult Create([Bind(Include = "Contents,RecipientString")] NewMessageViewModel newMessage)
        {
            if (newMessage != null && newMessage.RecipientString != null)
            {
                if (ModelState.IsValid)
                {
                    List<string> recipientList = newMessage.RecipientString.Split(';').ToList();
                    MS_SendMessageDTO newMessageDTO = new MS_SendMessageDTO()
                    {
                        SenderNo = GetUserIdentity(),
                        Contents = newMessage.Contents,
                        Recipients = recipientList
                    };
                    msFacade.sendMessageToRecipient(newMessageDTO);
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError("", "Unable to send entered message, please try again.");
                return View(new NewMessageViewModel() { Contents = newMessage.Contents, RecipientString = newMessage.RecipientString });
            }
            return View( new NewMessageViewModel() );
        }

        private string GetUserIdentity()
        {
            if (User != null && User.Identity != null)
                return User.Identity.Name;
            return "P44_54"; //Test user
        }
    }
}