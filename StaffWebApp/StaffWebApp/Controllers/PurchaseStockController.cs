﻿using CreditCardServiceFacadeProject;
using StaffWebApp.ViewModels.ThirdPartyStock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ThirdPartyFacades;
using ThirdPartyFacades.BazzasBazaarService;
using ThirdPartyFacades.DTOs;
using ThirdPartyFacades.DTOs.UnderCutters;
using WarehouseServiceFacadeProject;

namespace StaffWebApp.Controllers
{
    public class PurchaseStockController : Controller
    {
        private IWarehouseServiceFacade whFacade;
        private ICreditCardServiceFacade ccFacade;

        public PurchaseStockController(IWarehouseServiceFacade _whFacade, ICreditCardServiceFacade _ccFacade, IUnderCutterFacade _ucFacade, IDodgyDealersFacade _ddFacade, IStore _bbStore)
        {
            whFacade = _whFacade;
            ccFacade = _ccFacade;
            ThirdPartyFacadeHandler.ucFacade = _ucFacade;
            ThirdPartyFacadeHandler.ddFacade = _ddFacade;
            //ThirdPartyFacadeHandler.bbFacade = _bbStore; //No test facade has been made for this service.
        }

        // GET: PurchaseStock
        [Authorize]
        public ActionResult Index(string tpService = "", string ean = "")
        {
            ViewBag.ThirdPartyServices = ThirdPartyFacadeHandler.returnViewBagList();
            ViewBag.ErrorMessage = "";
            IEnumerable<Combined_Product> returnedData = new List<Combined_Product>();

            if (tpService != null && tpService != "") //Filter by Service
            {
                returnedData = ThirdPartyFacadeHandler.getProducts(tpService);
                if (returnedData != null && returnedData.Count() <= 0)
                {
                    ViewBag.ErrorMessage = "We are unable to communicate with that service at the moment. Please try again later.";
                }
            }

            if (ean != null && ean != "") //Filter by EAN
            {
                if (tpService != null && tpService != "")
                    returnedData = returnedData.Where(x => x.Ean == ean);
            }

            if (returnedData.Count() > 0)
                return View(returnedData.Select(x => new ThirdPartyStockViewModel()
                {
                    Name = x.Name,
                    Ean = x.Ean,
                    Description = x.Description,
                    BrandName = x.BrandName,
                    ProductID = x.Id,
                    Price = x.PriceForOne,
                    CategoryName = x.CategoryName,
                    Provider = x.Provider
                }).ToList());

            return View(new List<ThirdPartyStockViewModel>());
        }

        // GET: PurchaseStock/Details/5
        [Authorize]
        public ActionResult Details(int? id, string tpService)
        {
            if (id == null || tpService == null || tpService == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var detailedProduct = ThirdPartyFacadeHandler.getProductsByID(Int32.Parse(id.ToString()), tpService);
            if (detailedProduct == null)
                return HttpNotFound();

            return View(new ThirdPartyStockViewModel()
            {
                BrandName = detailedProduct.BrandName,
                CategoryName = detailedProduct.CategoryName,
                Description = detailedProduct.Description,
                Ean = detailedProduct.Ean,
                Name = detailedProduct.Name,
                Price = detailedProduct.PriceForOne,
                ProductID = detailedProduct.Id,
                Provider = detailedProduct.Provider
            });
        }

        // GET: PurchaseStock/PlaceOrder/5
        [Authorize(Roles = "CanPurchaseFromThirdParty")]
        public ActionResult PlaceOrder(int? id, string tpService)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var detailedProduct = ThirdPartyFacadeHandler.getProductsByID(Int32.Parse(id.ToString()), tpService);
            if (detailedProduct == null)
                return HttpNotFound();

            return View(new ThirdPartyOrderStockViewModel()
            {
                Name = detailedProduct.Name,
                Description = detailedProduct.Description,
                BrandName = detailedProduct.BrandName,
                CategoryName = detailedProduct.CategoryName,
                Ean = detailedProduct.Ean,
                Price = detailedProduct.PriceForOne,
                PricePer10 = detailedProduct.PriceForTen,
                ProductID = detailedProduct.Id,
                Seller = tpService,
                CreditCards = ccFacade.getCreditCards()
            });


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "CanPurchaseFromThirdParty")]
        public ActionResult PlaceOrder([Bind(Include = "Name,Description,BrandName,CategoryName,Ean,Price,PricePer10,ProductID,Seller,Quantity,SelectedCreditCardNumber")] ThirdPartyOrderStockViewModel orderVM)
        {
            if (ModelState.IsValid && orderVM != null)
            {
                var cardObj = ccFacade.getCreditCards().Where(x => x.hashCard == orderVM.SelectedCreditCardNumber).FirstOrDefault();
                if (cardObj != null)
                {
                    try
                    {
                        bool placed = ThirdPartyFacadeHandler.placeOrder(new Combined_Order()
                        {
                            ProductId = orderVM.ProductID,
                            AccountName = cardObj.nameOnCard,
                            CardNumber = cardObj.rawCardNumber,
                            Quantity = orderVM.Quantity,
                        }, orderVM.Seller);

                        if (placed)
                        {
                            List<WarehouseServiceFacadeProject.DTOs.WH_ProductStockUpdate> newStockItem = new List<WarehouseServiceFacadeProject.DTOs.WH_ProductStockUpdate>()
                            {
                                new WarehouseServiceFacadeProject.DTOs.WH_ProductStockUpdate()
                                {
                                    ProductName = orderVM.Name,
                                    ProductDescription = orderVM.Description,
                                    BrandName = orderVM.BrandName,
                                    CategoryName = orderVM.CategoryName,
                                    Ean = orderVM.Ean,
                                    StockToAdd = orderVM.Quantity,
                                    Price = orderVM.Price
                                },
                            };
                            whFacade.addProductStock(newStockItem);
                            return RedirectToAction("Index", new { tpService = orderVM.Seller, ean = orderVM.Ean });
                        }
                    }
                    catch (Exception ex) { } // Try catch to catch expection thrown manually.
                }
            }
            return View(orderVM);
        }
    }
}
