﻿using AutoMapper;
using StaffWebApp.ViewModels.Stock;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WarehouseServiceFacadeProject;
using WarehouseServiceFacadeProject.DTOs;

namespace StaffWebApp.Controllers
{
    [Authorize(Roles = "Staff")]
    /// <summary>
    /// Stock Controller. Contains methods to retrieve stock details from facades and construct them into viewmodels to be passed to the current stock pages.
    /// </summary>
    public class StockController : Controller
    {
        /// <summary>
        /// The Warehouse facade. Contains methods to retrieve warehouse data from the WarehouseService.
        /// </summary>
        private readonly IWarehouseServiceFacade whFacade;

        /// <summary>
        /// The AutoMapper object. Used to map objects from one type to another.
        /// </summary>
        private IMapper iMapper;

        /// <summary>
        /// The AutoMapper config. Defines what objects can be mapped into what other objects.
        /// </summary>
        private AutoMapper.MapperConfiguration mapperConfig;

        public StockController(IWarehouseServiceFacade _whFacade)
        {
            whFacade = _whFacade;
            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WH_Product, WarehouseStockViewModel>();
                cfg.CreateMap<WH_PriceHistory, PriceHistoryViewModel>();
                cfg.CreateMap<WarehouseStockViewModel, WH_ProductPriceUpdate>();
            });
            iMapper = mapperConfig.CreateMapper();
        }

        #region CurrentStockPage
        // GET: Stock
        [Authorize(Roles= "CanViewStockLevel")]
        public ActionResult Index()
        {
            List<ViewModels.Stock.WarehouseStockViewModel> warehouseVMs = new List<ViewModels.Stock.WarehouseStockViewModel>();
            var productsList = whFacade.getProducts();
            if (productsList != null && productsList.Count() > 0)
                warehouseVMs = productsList.Select(x => iMapper.Map<WH_Product, ViewModels.Stock.WarehouseStockViewModel>(x)).ToList();

            return View(warehouseVMs);
        }

        // GET: Stock/Edit/5
        [Authorize(Roles = "CanEditProducts")]
        public ActionResult Edit(string ean = "")
        {
            if (ean == null || ean == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            WH_Product product = whFacade.getProductByEAN(ean);
            if (product == null)
                return HttpNotFound();

            return View(iMapper.Map<WH_Product, WarehouseStockViewModel>(product));
        }

        [HttpPost]
        [Authorize(Roles = "CanEditProducts")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,Ean,ProductName,BrandName,CategoryName,ProductDescription,Price,StockLevel,RestockWarningLevel")] WarehouseStockViewModel stockVM)
        {
            if (ModelState.IsValid && stockVM != null)
            {
                var editedProduct = iMapper.Map<WarehouseStockViewModel, WH_ProductPriceUpdate>(stockVM);
                var successful = whFacade.editProduct(editedProduct);
                if(successful)
                    return RedirectToAction("Index");
                else
                    ModelState.AddModelError("", "Unable to edit that product at the moment, please try again later.");
            }
            return View(stockVM);
        }

        [Authorize(Roles = "CanViewProductPriceHistory")]
        public ActionResult PriceHistory(string ean = "")
        {
            if(ean == null || ean == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            IEnumerable<WH_PriceHistory> product = whFacade.getPriceHistoryByEAN(ean);
            if (product == null)
                return HttpNotFound();

            List<PriceHistoryViewModel> vmList = product.Select(x => iMapper.Map<WH_PriceHistory, PriceHistoryViewModel>(x)).ToList();
            return View(vmList);
        }
        #endregion
    }
}
