﻿using AutoMapper;
using CustomerProfileServiceFacadeProject;
using CustomerProfileServiceFacadeProject.DTOs;
using MessageServiceFacadeProject;
using MessageServiceFacadeProject.DTOs;
using PurchasingServiceFacadeProject;
using PurchasingServiceFacadeProject.DTOs;
using StaffWebApp.ViewModels.Customer;
using StaffWebApp.ViewModels.Customer.Orders;
using StaffWebApp.ViewModels.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace StaffWebApp.Controllers
{
    [Authorize(Roles = "Staff")]
    /// <summary>
    /// Customer Controller. Contains methods to retrieve customer details from facades and construct them into viewmodels to be passed to the customer profile pages.
    /// </summary>
    public class CustomerController : Controller
    {
        /// <summary>
        /// The Customer Profile facade. Contains methods to retrieve customer details from the CustomerProfileService.
        /// </summary>
        private ICustomerProfileServiceFacade cpFacade;

        /// <summary>
        /// The purchasing service facade. Contains methods to retrieve order and invoice details from the PurchasingService.
        /// </summary>
        private IPurchasingServiceFacade psFacade;

        /// <summary>
        /// The messaging service facade. Contains methods to retrieve messages and send messages using the MessagingService.
        /// </summary>
        private IMessageServiceFacade msFacade;

        /// <summary>
        /// The AutoMapper object. Used to map objects from one type to another.
        /// </summary>
        private IMapper iMapper;

        /// <summary>
        /// The AutoMapper config. Defines what objects can be mapped into what other objects.
        /// </summary>
        private AutoMapper.MapperConfiguration mapperConfig;

        //Used for testing the controller, isolating the app from services.
        public CustomerController(ICustomerProfileServiceFacade _cpFacade, IPurchasingServiceFacade _psFacade, IMessageServiceFacade _msFacade)
        {
            cpFacade = _cpFacade;
            psFacade = _psFacade;
            msFacade = _msFacade;

            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CP_CustomerProfile, CustomerProfileViewModel>().ForMember(destination => destination.CustomerDOB, opts => opts.MapFrom(source => source.CustomerDOB.ToString("dd/MM/yyyy")));
                cfg.CreateMap<PC_Orders, CustomerOrdersViewModel>();
                cfg.CreateMap<MS_Messages, MessageViewModel>();
            });
            iMapper = mapperConfig.CreateMapper();
        }

        // GET: Customer
        [Authorize(Roles = "CanViewCustomerProfile")]
        public ActionResult Index(string customerNo = "", string firstName = "", string lastName = "")
        {
            ViewBag.ErrorMessage = "";
            List<CustomerProfileViewModel> customerVMs = new List<CustomerProfileViewModel>();
            IEnumerable<CP_CustomerProfile> customerList = null;

            if (customerNo == "" && firstName == "" && lastName == "")
                customerList = cpFacade.getCustomers();
            else
                customerList = cpFacade.getCustomersFiltered(customerNo, firstName, lastName);

            if (customerList != null && customerList.Count() > 0)
                customerVMs = customerList.Select(x => iMapper.Map<CP_CustomerProfile, CustomerProfileViewModel>(x)).ToList();
            else if (customerList == null)
                ViewBag.ErrorMessage = "We are unable to communicate with that service at the moment. Please try again later.";

            return View(customerVMs);
        }

        // GET: Customer/Profile/C1001 1400
        [Authorize(Roles = "CanViewCustomerProfile")]
        public ActionResult Profile(string customerNo)
        {
            if (customerNo == null || customerNo == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            CP_CustomerProfile customer = cpFacade.getCustomersFiltered(customerNo, "", "").FirstOrDefault();
            if (customer == null)
                return HttpNotFound();

            return View(iMapper.Map<CP_CustomerProfile, CustomerProfileViewModel>(customer));
        }

        // GET: Customer/Edit/C1001 1400
        [Authorize(Roles = "CanEditCustomerPerms")]
        public ActionResult EditPerms(string customerNo)
        {
            if (customerNo == null || customerNo == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CP_CustomerProfile customer = cpFacade.getCustomersFiltered(customerNo, null, null).FirstOrDefault();
            if (customer == null)
                return HttpNotFound();

            return View(iMapper.Map<CP_CustomerProfile, CustomerProfileViewModel>(customer));
        }

        // POST: Customer/Edit/C1001 1400
        [HttpPost]
        [Authorize(Roles = "CanEditCustomerPerms")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPerms([Bind(Include = "CustomerNo,CustomerFName,CustomerLName,CustomerDOB,CustomerGender,CustomerApproved,CustomerCanPurchase")] CustomerProfileViewModel customerVM)
        {
            if (ModelState.IsValid && customerVM != null)
            {
                cpFacade.editCustomer(new CP_CustomerProfileUpdate()
                {
                    CustomerNo = customerVM.CustomerNo,
                    CustomerApproved = customerVM.CustomerApproved,
                    CustomerCanPurchase = customerVM.CustomerCanPurchase
                });
                return RedirectToAction("Index");
            }
            return View(customerVM);
        }

        // GET: Customer/Profile/OrderHistory/C1001 1400
        [Authorize(Roles = "CanViewCustomerOrderHistory")]
        public ActionResult OrderHistory(string customerNo)
        {
            if (customerNo == null || customerNo == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            IEnumerable<PC_Orders> orderList = psFacade.getOrdersFromCustomerNo(customerNo);
            if (orderList == null || orderList.Count() == 0)
                return HttpNotFound();

            List<CustomerOrdersViewModel> custOrderVM = orderList.Select(x => iMapper.Map<PC_Orders, CustomerOrdersViewModel>(x)).ToList();
            return View(custOrderVM);
        }

        // GET: Customer/Profile/MessageHistory/C1001 1400
        [Authorize(Roles = "CanViewCustomerMessageHistory")]
        public ActionResult MessageHistory(string customerNo)
        {
            if (customerNo == null || customerNo == "")
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            IEnumerable<MS_Messages> messageList = msFacade.getMessagesFromRecipientID(customerNo);
            if (messageList == null || messageList.Count() == 0)
                return View(new List<MessageViewModel>() { new MessageViewModel() { RecipientNo = customerNo } }); //Empty view model

            List<MessageViewModel> custMessageVM = messageList.Select(x => iMapper.Map<MS_Messages, MessageViewModel>(x)).ToList();
            if(custMessageVM != null)
                custMessageVM.ForEach(x => x.RecipientNo = customerNo);

            return View(custMessageVM);
        }
    }
}
