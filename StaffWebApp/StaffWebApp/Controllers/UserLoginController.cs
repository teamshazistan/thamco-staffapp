﻿using StaffProfileServiceFacadeProject;
using StaffProfileServiceFacadeProject.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace StaffWebApp.Controllers
{
    public class UserLoginController : Controller
    {
        private IStaffProfileServiceFacade spFacade;
        private IUserLoginWrapper usLogin;

        public UserLoginController(IStaffProfileServiceFacade _spFacade, IUserLoginWrapper _usLogin)
        {
            spFacade = _spFacade;
            usLogin = _usLogin;
        }

        // GET: UserLogin
        public ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult Register(ViewModels.Account.AccountRegister user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var staffProfile = spFacade.getStaffProfileFromLogin(user.Email);
        //        if(staffProfile == null)
        //        {
        //            spFacade.registerNewStaff(new StaffProfileServiceFacadeProject.DTOs.SP_StaffProfile()
        //            {
        //                Email = user.Email,
        //                PasswordHash = SHA1.Encode(user.Password),
        //            });

        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "That email is already in use, please try another.");
        //        }
        //    }
        //    return View(user);
        //}

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ViewModels.Account.AccountLogin user)
        {     
            if (ModelState.IsValid && user != null)
            {
                var staffProfile = spFacade.getStaffProfileFromLogin(new SP_StaffLoginDTO() {Username = user.Username, Password = SHA1.Encode(user.Password) });
                if (staffProfile != null)
                {
                    if (usLogin.AuthenticateUser(user.Username, user.RememberMe) && usLogin.GenerateCookies(staffProfile, spFacade))
                        return RedirectToAction("Index", "Home");
                    else
                        Logout(); //Authentication may have passed but cookie generation did not, in that case we want to logout user to prevent issues.
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            usLogin.SignOutUser();
            return RedirectToAction("Index", "Home");
        }
    }
}